import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;


public class BazaTest {

	Baza baza = new Baza();
	
	@Test
	public void testGetConnecion() 
	{
		assertNotNull(baza.getConnection());
	}
	
	@Test
	public void testToString()
	{
		assertNotNull(baza.toString());
		System.out.println(baza.toString());
	}
	
	@Test
	public void testEntityToObject()
	{
		Pracownik p = baza.selectEntityAsObject(4);
		assertEquals(4,p.getId());
		assertEquals("Lidia",p.getImie());
		assertEquals("Konieczna",p.getNazwisko());
		assertEquals(1,p.getDzial());
		assertEquals(1500,p.getZarobki());
	}
	
	@Test
	public void testAddEmployee()
	{
		baza.addEmployee("Helena", "Zamojska", 1, 1200);
		Pracownik p = baza.selectEntityAsObject(baza.selectLastId());
		assertEquals("Helena", p.getImie());
		assertEquals("Zamojska", p.getNazwisko());
		assertEquals(1, p.getDzial());
		assertEquals(1200, p.getZarobki());
	}
	
	@Test
	public void testRemoveEmployee()
	{
		baza.removeEmployee(11);
		Pracownik p = baza.selectEntityAsObject(11);
		assertEquals(null, p.getImie());
		assertEquals(null, p.getNazwisko());
		assertEquals(0, p.getDzial());
		assertEquals(0, p.getZarobki());
	}
	
	@Test
	public void testUpdateEmployee()
	{
		baza.updateEmployee(13, "Jadwiga");
		Pracownik p = baza.selectEntityAsObject(13);
		assertEquals("Jadwiga",p.getImie());
	}
}
