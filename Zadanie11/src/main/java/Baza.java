import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;


public class Baza {
	
	private Connection connection = null;
	private String user;
	private String password;
	private String url;
	
	public Baza()
	{
		this.user = new String("kompot");
		this.password = new String ("topmok");
		this.url = new String("jdbc:mysql://localhost/kompot");
	}
	
	public Baza(String user, String password, String url)
	{
		this.user = user;
		this.password = password;
		this.url = url;
	}
	
	public Connection getConnection()
	{
	    Connection connection = null;
		try 
		{
		    connection = DriverManager.getConnection(this.url,this.user,this.password);
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		
	    return connection;
	}
	
	public void addEmployee(String firstName, String lastName, int department, int salary)
	{
		try
		{
			Statement statement = this.getConnection().createStatement();
			statement.executeUpdate("INSERT INTO pracownicy VALUES (null,'" + firstName +"','"
					+ lastName + "'," + department + "," + salary + ")");
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	public void removeEmployee(int id)
	{
		try
		{
			Statement statement = this.getConnection().createStatement();
			statement.executeUpdate("DELETE FROM pracownicy WHERE id=" + id);
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	public void updateEmployee(int id, String firstName)
	{
		try
		{
			String query = "UPDATE pracownicy SET imie = ? WHERE id = ?";
			java.sql.PreparedStatement prepStatement = this.getConnection().prepareStatement(query);
			prepStatement.setString(1, firstName);
			prepStatement.setInt(2, id);
			prepStatement.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	public void updateEmployee(int id, String firstName, String lastName, int department, int salary)
	{
		try
		{
			Statement statement = this.getConnection().createStatement();
			statement.executeUpdate("UPDATE pracownicy SET imie='" + firstName
					+ "',nazwisko='" + lastName + "',dzial=" + department
					+ ",zarobki=" + salary + " WHERE id=" + id);
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	public Pracownik selectEntityAsObject(int id)
	{
		Pracownik pracownik = new Pracownik();
		try
		{
			Statement statement = this.getConnection().createStatement();
			ResultSet results = statement.executeQuery("SELECT * FROM pracownicy WHERE id = " + id);
			while(results.next())
			{
				pracownik.setId(results.getInt("id"));
				pracownik.setImie(results.getString("imie"));
				pracownik.setNazwisko(results.getString("nazwisko"));
				pracownik.setDzial(results.getInt("dzial"));
				pracownik.setZarobki(results.getInt("zarobki"));
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		return pracownik;
	}
	
	public String toString()
	{
		String text = new String("");
		try
		{
			Statement statement = this.getConnection().createStatement();
			ResultSet results = statement.executeQuery("SELECT * FROM pracownicy");
			while(results.next())
			{
				text += results.getInt("id") + " "
						+ results.getString("imie") + " "
						+ results.getString("nazwisko") + " "
						+ results.getInt("dzial") + " "
						+ results.getInt("zarobki")
						+ "\n";
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		return text;
	}
	
	public int selectLastId()
	{
		int id = 0;
		try
		{
			Statement statement = this.getConnection().createStatement();
			ResultSet results = statement.executeQuery("SELECT id FROM pracownicy");
			results.last();
			id = results.getInt("id");
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		return id;
	}
}
