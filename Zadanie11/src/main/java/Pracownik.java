
public class Pracownik 
{
	//Pola
	private int id;
	private String imie;
	private String nazwisko;
	private int dzial;
	private int zarobki;
	
	//Akcesory
	public int getId()
	{
		return this.id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public String getImie()
	{
		return this.imie;
	}
	
	public void setImie(String imie)
	{
		this.imie = imie;
	}
	
	public String getNazwisko()
	{
		return this.nazwisko;
	}
	
	public void setNazwisko(String nazwisko)
	{
		this.nazwisko = nazwisko;
	}
	
	public int getDzial()
	{
		return this.dzial;
	}
	
	public void setDzial(int dzial)
	{
		this.dzial = dzial;
	}

	public int getZarobki()
	{
		return this.zarobki;
	}
	
	public void setZarobki(int zarobki)
	{
		this.zarobki = zarobki;
	}
}
