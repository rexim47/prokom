

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;

import javax.swing.DefaultListModel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JOptionPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import java.awt.event.ActionListener;

import javax.swing.JList;
import javax.swing.JSplitPane;
import javax.swing.JButton;
import javax.swing.JInternalFrame;

import java.awt.Component;

import javax.swing.Box;
import javax.swing.JTextField;

import java.awt.Button;

import javax.swing.SwingConstants;
import javax.swing.BoxLayout;

import java.awt.FlowLayout;

import javax.swing.JScrollPane;

import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.Date;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Color;

public class Kalendarz extends JFrame {
	
	private JPanel contentPane;
	private JCalendar calendar;
	private DefaultListModel eventsListModel;
	private JList eventsList;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Kalendarz frame = new Kalendarz();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Kalendarz() {
		setTitle("Kalendarz");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 450);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMenu = new JMenu("Menu");
		menuBar.add(mnMenu);
		
		JMenuItem mntmSettings = new JMenuItem("Ustawienia");
		mnMenu.add(mntmSettings);
		
		JMenuItem mntmSaveToDB = new JMenuItem("Zapis do Bazy Danych");
		mnMenu.add(mntmSaveToDB);
		
		JMenuItem mntmLoadFromDB = new JMenuItem("Odczyt z Bazy Danych");
		mnMenu.add(mntmLoadFromDB);
		
		JMenuItem mntmSaveToXML = new JMenuItem("Zapis do XML");
		mnMenu.add(mntmSaveToXML);
		
		JMenuItem mntmLoadFromXML = new JMenuItem("Odczyt z XML");
		mnMenu.add(mntmLoadFromXML);
		
		JMenuItem mntmSaveToOutlook = new JMenuItem("Eksport do Outlooka");
		mnMenu.add(mntmSaveToOutlook);
		
		JMenu mnHelp = new JMenu("Pomoc");
		menuBar.add(mnHelp);
		
		JMenuItem mntmAbout = new JMenuItem("O programie");
		mnHelp.add(mntmAbout);
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(contentPane, "Organizer\nProjekt na Programowanie Komponentowe\nAutorzy:\nAdrian Majzer 189723\nJakub Pietrzak 189749","O programie",JOptionPane.INFORMATION_MESSAGE);
			}
		});

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel eventsPane = new JPanel();
		contentPane.add(eventsPane, BorderLayout.WEST);
		eventsPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JScrollPane scrollPane = new JScrollPane();
		eventsPane.add(scrollPane);
		
		eventsListModel = new DefaultListModel();
		eventsList = new JList(eventsListModel);
		eventsList.setVisibleRowCount(20);
		scrollPane.setViewportView(eventsList);
		
		JPanel calendarPane = new JPanel();
		contentPane.add(calendarPane, BorderLayout.CENTER);
		calendarPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		calendar = new JCalendar();
		calendar.getDayChooser().getDayPanel().setPreferredSize(new Dimension(280, 310));
		calendarPane.add(calendar);
		
		JPanel buttonsPane = new JPanel();
		contentPane.add(buttonsPane, BorderLayout.SOUTH);
		
		JButton btnAdd = new JButton("Dodaj");
		
		JButton btnRefresh = new JButton("Od\u015Bwierz");
		buttonsPane.add(btnRefresh);
		buttonsPane.add(btnAdd);
		
		JButton btnEdit = new JButton("Edytuj");
		buttonsPane.add(btnEdit);
		
		JButton btnRemove = new JButton("Usu\u0144");
		buttonsPane.add(btnRemove);
	}
}
