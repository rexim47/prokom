
public class PascalTriangle
{

	private int[][] data;
	public void compute(int n) throws Exception
	{		
		if(n<=0)
			throw new Exception("Liczba wierszy musi byc dodatnia");
		else
		{
			//Tworzenie tablicy tr�jk�tnej
			data = new int[n][0];
			for(int i=0;i<n; i++)
				data[i]=new int [i+1];
			
			//Wpisanie 1 elementu
			data[0][0]=1;
			
			//Wypelnianie wierszy
			for(int i=1; i<n; i++)
			{
				for(int k=0; k<=i; k++)
				{
					if(k==0 || k==i)
						data[i][k]=1;
					else
						data[i][k]=data[i-1][k-1]+data[i-1][k];
					//data[counter]=dwumian(i,k);
				}
			}
		}
	}
	public int silnia(int n)
	{
		if(n==0 || n==1)
			return 1;
		return n*silnia(n-1);
	}
	public int dwumian(int n, int k)
	{
		return silnia(n)/(silnia(k)*silnia(n-k));
	}
	
	public int[][] getData()
	{
		return data;
	}
}
