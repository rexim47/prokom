import static org.junit.Assert.*;

import org.junit.Test;


public class PascalTriangleTest {

	PascalTriangle pt = new PascalTriangle();
	//public int [][] n0 = new int[0][0];
	public int [][] n1 = new int[1][1];
	public int [][] n4 = new int[4][0];
	
	@Test
	public void test0() {
		try
		{
			pt.compute(0);
		}catch(Exception e){ System.out.println(e.getMessage());}
		//assertArrayEquals(n0,pt.getData());
	}
	
	@Test
	public void test1() {
		n1[0][0] = 1;
		try
		{
			pt.compute(1);
		}catch(Exception e){ System.out.println(e.getMessage());}
		assertArrayEquals(n1,pt.getData());
	}
	
	@Test
	public void test4() {
		n4[0] = new int[1];
		n4[0][0] = 1;
		n4[1] = new int[2];
		n4[1][0] = 1;
		n4[1][1] = 1;
		n4[2] = new int[3];
		n4[2][0] = 1;
		n4[2][1] = 2;
		n4[2][2] = 1;
		n4[3] = new int[4];
		n4[3][0] = 1;
		n4[3][1] = 3;
		n4[3][2] = 3;
		n4[3][3] = 1;
		try
		{
			pt.compute(4);
		}catch(Exception e){ System.out.println(e.getMessage());}
		assertArrayEquals(n4,pt.getData());
	}
	
	@Test
	public void testMinus3() {
		try
		{
			pt.compute(-3);
		}catch(Exception e){ System.out.println(e.getMessage());}
	}
	
	@Test
	public void testWypisania() 
	{
		try
		{
			pt.compute(4);
		}catch(Exception e){ System.out.println(e.getMessage());}
		for(int i = 0; i<4; i++)
		{
			for(int j = 0; j<=i; j++)
			{
				System.out.print(pt.getData()[i][j]);
			}
			System.out.println();
		}
	}
}
