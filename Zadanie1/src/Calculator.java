
public class Calculator 
{
	//Metody typu int
	public static int add(int a, int b)
	{
		return a+b;
	}
	
	public static int difference(int a, int b)
	{
		return a-b;
	}
	
	public static int multiply(int a, int b)
	{
		return a*b;
	}
	
	public static int divide(int a, int b)
	{
		return a/b;
	}
	
	//Metody typu double
	public static double add(double x, double y)
	{
		return x+y;
	}
	
	public static double difference(double x, double y)
	{
		return x-y;
	}
	
	public static double multiply(double x, double y)
	{
		return x*y;
	}
	
	public static double divide(double x, double y)
	{
		return x/y;
	}
}
