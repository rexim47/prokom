import static org.junit.Assert.*;

import org.junit.Test;


public class CalculatorTest 
{
	//Utworzenie obiektu klasy Calculator
	Calculator c = new Calculator();
	
	//Integer
	//Przekroczenie maksymalnego zakresu (overflow)
	@Test
	public void testIntOverflow() 
	{
		assertEquals(Integer.MIN_VALUE,c.add(Integer.MAX_VALUE,1));
	}
	
	//Przekroczenie minimalnego zakresu (underflow)
	@Test
	public void testIntUnderflow() 
	{
		assertEquals(Integer.MAX_VALUE,c.difference(Integer.MIN_VALUE,1));
	}
	
	//Double
	//Przekroczenie maksymalnego zakresu (overflow)
	@Test
	public void testDoubleOverflow() 
	{
		assertEquals(Double.MAX_VALUE,c.add(Double.MAX_VALUE,1.0),0.0);
	}
	//Przekroczenie minimalnego zakresu (underflow)
	@Test
	public void testDoubleUnderflow() 
	{
		assertEquals(-1.0,c.difference(Double.MIN_VALUE,1),0.0);
	}
	
	//Dzielenie przez zero
	//Dla warto�ci typu int otrzymamy 
	//java.lang.ArithmeticException: / by zero
	@Test
	public void testDivideByZero()
	{
		assertEquals(Double.POSITIVE_INFINITY,c.divide(5.0,0.0),0.0);
	}
	
	@Test
	public void testNaN()
	{
		assertEquals(Double.NaN,c.divide(0.0, 0.0),0.0);
	}
	
	@Test
	public void testNegativeInfinity()
	{
		assertEquals(Double.NEGATIVE_INFINITY,c.divide(-5.0,0.0),0.0);
	}
	
	@Test
	public void testNegativeZero()
	{
		assertEquals(-0.0,c.multiply(0.0,-1.0),0.0);
	}
	
	@Test
	public void testNegativeZero2()
	{
		assertEquals(-0.0,c.multiply(0.0,1.0),0.0);
	}
	
	@Test
	public void testPositiveZero()
	{
		assertEquals(0.0,c.multiply(0.0,1.0),0.0);
	}
	
	@Test
	public void testPositiveZero2()
	{
		assertEquals(0.0,c.multiply(0.0,-1.0),0.0);
	}
}

