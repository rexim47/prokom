import javax.swing.*;

import java.awt.*;
import java.awt.event.*; 
import java.applet.*;


public class MyApplet2 extends Applet implements ItemListener
{  
	//Flagi wlaczenia/wylaczenia bokow trojkata
	boolean bokA = false,
			bokB = false,
			bokC = false;
	
	int fontSize = 18;
	int fontType = Font.PLAIN;
	Color fontColor = Color.BLACK;
	String fontName = "Arial";
	
	class KlasaWewnetrzna implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() == przycisk1) 
				bokA = !bokA; 
			if (e.getSource() == przycisk2) 
				bokB = !bokB; 
			if (e.getSource() == przycisk3)
				bokC = !bokC;
			repaint(); 	//Ponowne wywolanie funkcji paint
		}
	}
	
	class KlasaAdaptacyjna extends MouseAdapter //implements MouseListener
	{
		public void mousePressed(MouseEvent e){
			bokA = !bokA;
			bokB = !bokB;
			bokC = !bokC;
			repaint();
		}
	}
	
	class KlasaAdaptacyjna2 extends KeyAdapter
	{
		public void keyPressed(KeyEvent e){
			if(e.getKeyChar() == 'a')
				bokA = !bokA;
			if(e.getKeyChar() == 'b')
				bokB = !bokB;
			if(e.getKeyChar() == 'c')
				bokC = !bokC;
			repaint();
		}
	}
	
	public void paint (Graphics g) 
	{
		//Ustawienie wymiarow appletu
		setSize(500,500);
				
		if (bokA) 
		{
			g.setColor(this.getForeground()); 
			g.drawLine(50,60, 80, 190);
			przycisk1.setLabel("wylacz bokA");
		}
		else 
		{
			g.setColor(this.getBackground()); 
			g.drawLine(50,60, 80, 190);
			przycisk1.setLabel("WLACZ bokA");
		}
		
		if (bokB) 
		{
			g.setColor(Color.BLACK);
			g.drawLine(80, 190, 190, 60);
			przycisk2.setLabel("wylacz bokB");
		}
		else 
		{
			g.setColor(Color.WHITE); 
			g.drawLine(80, 190, 190, 60);
			przycisk2.setLabel("WLACZ bokB");
		}
		
		if (bokC) 
		{
			g.setColor(Color.BLACK);
			g.drawLine(190, 60, 50, 60);
			przycisk3.setLabel("wylacz bokC");
		}
		else 
		{
			g.setColor(Color.WHITE);
			g.drawLine(190, 50, 50, 50);
			przycisk3.setLabel("WLACZ bokC");
		}
		
		String tekst = new String("Tekst do wyświetlenia");
		Font font = new Font(fontName, fontType, fontSize);
		g.setFont(font);
		g.setColor(fontColor);
		g.drawString(tekst, 50, 350);
	}
	
	public void itemStateChanged(ItemEvent evt) 
	{	
		//Rozmiar czcionki
		if(evt.getItemSelectable() == radio1)
			fontSize = 18;
		if(evt.getItemSelectable() == radio2)
			fontSize = 24;
		if(evt.getItemSelectable() == radio3)
			fontSize = 32;
		if(evt.getItemSelectable() == radio4)
			fontSize = 36;
		
		//Wybor czcionki
		if(evt.getItemSelectable() == wyborCzcionki)
			fontName = ((Choice)evt.getItemSelectable()).getSelectedItem();
	
		repaint(); 	//Ponowne wywolanie funkcji paint
	}
	
	// Deklaracje obiektow
	//Przyciski
	Button 	przycisk1, przycisk2, przycisk3; 
	//Checkboxy
	Checkbox checkbox1, checkbox2, checkbox3, checkbox4, checkbox5;
	//Okno tekstowe
	TextField oknoTekstowe; 
	//Radioprzyciski
	Checkbox radio1, radio2, radio3, radio4;
	CheckboxGroup checkboxgroup;
	//Lista wyboru
	Choice wyborCzcionki;
	Choice wyborKoloru;
	
	public void init(){
		
		addMouseListener(new KlasaAdaptacyjna());
		addKeyListener(new KlasaAdaptacyjna2());
		
		KlasaWewnetrzna klasaWewnetrzna = new KlasaWewnetrzna();
		//Przyciski do wlaczania/wylaczania bokow trojkata
		przycisk1 = new Button("WLACZ A");	// inicjalizacja przycisku
		add(przycisk1);						// wyswietla przycisk w oknie appletu
		przycisk1.addActionListener(klasaWewnetrzna);	// przypisuje przyciskowi dzialanie opisane w actionPerformed
		
		przycisk2 = new Button("WLACZ B");	
		add(przycisk2);
		przycisk2.addActionListener(klasaWewnetrzna);	
		
		przycisk3 = new Button("WLACZ C");	
		add(przycisk3);						
		przycisk3.addActionListener(klasaWewnetrzna);
		
		class KlasaLokalna implements ItemListener
		{

			public void itemStateChanged(ItemEvent e) {
				//Wlaczanie/wylaczanie bokow za pomoca checkboxow
				if(e.getItemSelectable() == checkbox1)
					bokA = !bokA;
				if(e.getItemSelectable() == checkbox2)
					bokB = !bokB;
				if(e.getItemSelectable() == checkbox3)
					bokC = !bokC;
				repaint();
			}
		}
		
		KlasaLokalna klasaLokalna = new KlasaLokalna();
		
		//Checkboxy do wlaczania/wylaczania bokow trojkata
		checkbox1 = new Checkbox("A");
		add(checkbox1);
		checkbox1.addItemListener(klasaLokalna);
		
		checkbox2 = new Checkbox("B");
		add(checkbox2);
		checkbox2.addItemListener(klasaLokalna);
		
		checkbox3 = new Checkbox("C");
		add(checkbox3);
		checkbox3.addItemListener(klasaLokalna);
		
		KlasaZewnetrzna klasaZewnetrzna = new KlasaZewnetrzna(this);
		//Checkboxy do wyboru rodzaju czcionki
		checkbox4 = new Checkbox("Italic");
		add(checkbox4);
		checkbox4.addItemListener(klasaZewnetrzna);
		
		checkbox5 = new Checkbox("Bold");
		add(checkbox5);
		checkbox5.addItemListener(klasaZewnetrzna);
		
		//Radio przyciski do ustalania rozmiaru czcionki
		checkboxgroup = new CheckboxGroup();
		radio1 = new Checkbox("18pt",checkboxgroup,true);
		add(radio1);
		radio1.addItemListener(this);
		
		radio2 = new Checkbox("24pt",checkboxgroup,false);
		add(radio2);
		radio2.addItemListener(this);
		
		radio3 = new Checkbox("32pt",checkboxgroup,false);
		add(radio3);
		radio3.addItemListener(this);
		
		radio4 = new Checkbox("36pt",checkboxgroup,false);
		add(radio4);
		radio4.addItemListener(this);
		
		//Lista wyboru czcionek
		wyborCzcionki = new Choice();
		wyborCzcionki.add("Arial");
		wyborCzcionki.add("Comic");
		wyborCzcionki.add("Courier New");
		wyborCzcionki.add("Lucida");
		wyborCzcionki.add("Tahoma");
		wyborCzcionki.add("Times New Roman");
		wyborCzcionki.add("Verdana");
		add(wyborCzcionki);
		wyborCzcionki.addItemListener(this);
		
		//Lista wyboru kolorow
		wyborKoloru = new Choice();
		wyborKoloru.add("Czarny");
		wyborKoloru.add("Czerwony");
		wyborKoloru.add("Zielony");
		wyborKoloru.add("Niebieski");
		add(wyborKoloru);
		//Klasa anonimowa
		wyborKoloru.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e){
				//Wybor koloru czcionki
				if(e.getItemSelectable() == wyborKoloru)
				{
					if(((Choice)e.getItemSelectable()).getSelectedItem() == "Czarny")
						fontColor = Color.BLACK;
					if(((Choice)e.getItemSelectable()).getSelectedItem() == "Czerwony")
						fontColor = Color.RED;
					if(((Choice)e.getItemSelectable()).getSelectedItem() == "Zielony")
						fontColor = Color.GREEN;
					if(((Choice)e.getItemSelectable()).getSelectedItem() == "Niebieski")
						fontColor = Color.BLUE;
				}
				repaint(); 	//Ponowne wywolanie funkcji paint
			}
		});
	}
}