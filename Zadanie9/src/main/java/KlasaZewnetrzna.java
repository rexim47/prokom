import java.applet.Applet;
import java.awt.Button;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;


public class KlasaZewnetrzna extends Applet implements ItemListener
{
	MyApplet2 applet;
	
	KlasaZewnetrzna(MyApplet2 applet)
	{
		this.applet = applet;
	}
	
	public void itemStateChanged(ItemEvent e) {
		//Rodzaj czcionki
		if(applet.checkbox4.getState() == true)
			applet.fontType = Font.ITALIC;
		if(applet.checkbox5.getState() == true)
			applet.fontType = Font.BOLD;
		if(applet.checkbox4.getState() == true && applet.checkbox5.getState() == true)
			applet.fontType = Font.ITALIC + Font.BOLD;
		if(applet.checkbox4.getState() == false && applet.checkbox5.getState() == false)
			applet.fontType = Font.PLAIN;
		applet.repaint(); 	//Ponowne wywolanie funkcji paint
	}
}
