public class Main 
{
	public static double main(String[] args) throws Exception
	{
		if(args.length>2)
		{	
//			// drukowanie Pierwszego argumentu wywolania 
//			System.out.println(args[0]); 
//			if(args.length>2)
//			{
//				// drukowanie DRUGIEGO i TRZECIEGO argumentu wywolania 
//				System.out.println(args[1]+" | "+args[2]);
//			}else{throw new Exception("Za malo argumentow");}
//			// Zabezpieczenie przed niewlasciwa liczb� argument�w
//			if (args.length <3) throw new Exception("Za malo argumentow");
//			else System.out.println(" * " + args[0]+" ** "+args[1]+" *** "+args[2]);
//			// Drukowanie kazdej podanej ilosci argumentow
//			for (int i=0; i<args.length; i++) // ilosc argumentow jako ograniczenie gorne petli
//			System.out.println(args[i]); 
			
			//Druga czesc
			boolean checkArgs = true;
			try
			{
				Double.valueOf(args[0]);
				Double.valueOf(args[1]);
			}
			catch(NumberFormatException e)
			{
				checkArgs = false;
			}
			if(checkArgs)
			{
				//System.out.print(args[0] + " + " + args[1] + " = " );
				if(args[2]=="+")
					//System.out.println(Calculator.add(Double.valueOf(args[0]),Double.parseDouble(args[1])));
					return Calculator.add(Double.valueOf(args[0]),Double.parseDouble(args[1]));
				else if(args[2]=="-")
					//System.out.println(Calculator.difference(Double.valueOf(args[0]),Double.parseDouble(args[1])));
					return Calculator.difference(Double.valueOf(args[0]),Double.parseDouble(args[1]));
				else if(args[2]=="*")
//					System.out.println(Calculator.multiply(Double.valueOf(args[0]),Double.parseDouble(args[1])));
					return Calculator.multiply(Double.valueOf(args[0]),Double.parseDouble(args[1]));
				else if(args[2]=="/")
					//System.out.println(Calculator.divide(Double.valueOf(args[0]),Double.parseDouble(args[1])));
					return Calculator.divide(Double.valueOf(args[0]),Double.parseDouble(args[1]));
				else
					//System.err.print("Nieprawidlowy znak operacji. Sprobuj +,-,*,/");
					throw new Exception("Nieprawidlowy znak operacji. Sprobuj +,-,*,/");
			}
			else
				System.err.println("Podane argumenty nie sa liczbami");
		}
		else
			throw new Exception("Za malo argumentow");
		return 0.0;
	}
}