import java.io.PrintStream;

public class Tablice 
{
	public static void takieSame(double [] tablica, int wartosc)
	{
		for(int i=0; i<tablica.length; i++)
			tablica[i] = wartosc;
	}
	
	public static void drukujTablice(PrintStream ciag, double [] tablica)
	{
		for (int i=0; i < tablica.length; i=i+1)
			ciag.print(tablica[i]+" "); 
		ciag.println("");
	}

	public static void drukujTablice(PrintStream ciag, int [] tablica)
	{
		for (int i=0; i < tablica.length; i=i+1)
			ciag.print(tablica[i]+" "); 
		ciag.println("");
	}
	
	public static void zalezyOdI(double [] tablica)
	{
		for(int i=0; i<tablica.length; i++)
			tablica[i]=i*2;
	}
	
	public static int[] dynamiczna(int rozmiar)
	{
		int tablica[] = new int[rozmiar];
		for(int i=0; i<rozmiar; i++)
			tablica[i]=i*i-i;
		return tablica;
	}
	
	public static int[] losowa(int rozmiar)
	{
		int tablica[] = new int[rozmiar];
		for(int i=0; i<rozmiar; i++)
			tablica[i]=(int)(Math.random()*20);
		return tablica;
	}
	
	public static int[] losowaZakres(int rozmiar, int min, int max)//zakres jako parametr
	{
		int tablica[] = new int[rozmiar];
		for(int i=0; i<rozmiar; i++)
			tablica[i]=(int)(Math.random()*(max-min) + min);
		return tablica;
	}
	
	public static void sortowanie(int [] tablica)
	{
		int rozmiar = tablica.length;
		int temp;
		boolean flaga;
		do
		{
			flaga=true;
			for(int i=0; i<rozmiar-1; i++)
			{
				if(tablica[i] > tablica[i+1])
				{
					temp=tablica[i];
					tablica[i]=tablica[i+1];
					tablica[i+1]=temp;
					flaga=false;
				}
			}
			rozmiar--;
		}while(flaga==false && rozmiar>1);
	}
	
	public static void random(int [] tablica)
	{
		for(int i=0; i<tablica.length; i++)
			tablica[i]=i%10*2;
	}
}
