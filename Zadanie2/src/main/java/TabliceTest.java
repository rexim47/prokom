import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Date;

import org.junit.Test;

public class TabliceTest 
{
	@Test
	public void testTakieSame()
	{
		double tab[] = new double[100];
		Tablice.takieSame(tab, 6);
		//Sprawdzenie czy elementy w tablicy s� takie same
		boolean flag = true; //true - s� takie same, false - rozne
			for(int i=0;i<tab.length-1;i++)
				if(tab[i] != tab[i+1])
					flag = false; //jakis element sie nie zgadza
		//Sprawdzamy czy sie zgadzaja
		assertEquals(true,flag);
	}
	
	@Test
	public void testDrukowanieInt()
	{
		int tab [] = Tablice.dynamiczna(3);
		Tablice.drukujTablice(System.out,tab);
	}
	
	@Test
	public void testDrukowanieDouble()
	{
		double tab [] = new double[3];
		Tablice.takieSame(tab,3);
		Tablice.drukujTablice(System.out,tab);
	}
	
	@Test
	public void testZalezyOdI()
	{
		double tab[] = new double[100];
		Tablice.zalezyOdI(tab);
		//Sprawdzenie czy elementy w tablicy s� takie same
		boolean flag = true; //true - s� takie same, false - rozne
			for(int i=0;i<tab.length;i++)
				if(tab[i] % 2 != 0)
					flag = false; //jakis element sie nie zgadza
		//Sprawdzamy czy sie zgadzaja
		assertEquals(true,flag);
	}
	
	@Test
	public void testDynamiczna()
	{
		int tab[] = Tablice.dynamiczna(56);
		assertEquals(56,tab.length);
	}
	
	@Test
	public void testLosowa()
	{
		int tab[] = Tablice.losowa(100);
		//Sprawdzenie czy elementy w tablicy s� z zadanego przedzialu
		boolean flag = true; //true - s� takie same, false - rozne
			for(int i=0;i<tab.length;i++)
				if(tab[i]<0 || tab[i]>20)
					flag = false; //jakis element sie nie zgadza
		//Sprawdzamy czy sie zgadzaja
		assertEquals(true,flag);
	}
	
	@Test
	public void testZakres()
	{
		int tab[] = Tablice.losowaZakres(100,1,10);
		//Sprawdzenie czy elementy w tablicy s� z zadanego przedzialu
		boolean flag = true; //true - s� takie same, false - rozne
			for(int i=0;i<tab.length;i++)
				if(tab[i]<1 || tab[i]>10)
					flag = false; //jakis element sie nie zgadza
		//Sprawdzamy czy sie zgadzaja
		assertEquals(true,flag);
	}
	
	@Test
	public void testSortowanie()
	{
		//Tworzenie
		int [] tablica = Tablice.losowa(100); //generujemy losowa tablice
		int [] tablica2 = tablica.clone(); //kopiujemy ta tablice
		
		//Sortowanie
		Tablice.sortowanie(tablica);
		Arrays.sort(tablica2);
		
		//Sprawdzenie czy posortowane tablice s� takie same
		boolean flag = true; //true - s� takie same, false - rozne
		for(int i=0;i<tablica.length;i++)
			if(tablica[i]!=tablica2[i])
				flag = false; //jakis element sie rozni
		
		//Sprawdzamy czy sie zgadzaja
		assertEquals(true,flag);
	}
	
	////TESTY SZYBKOSCI
	@Test
	public void maleSortowanie()
	{
		int [] tablica = Tablice.losowa(100); //generujemy losowa tablice
		int [] tablica2 = tablica.clone(); //kopiujemy ta tablice

		//Sprawdzamy czas sortowania b�belkowego
		Date czasStartBubble = new Date();
		Tablice.sortowanie(tablica);
		Date czasKoncBubble = new Date();
		long czasBubble = czasKoncBubble.getTime() - czasStartBubble.getTime();
		
		//Sprawdzamy czas sortowania z Arrays
		Date czasStartArrays = new Date();
		Arrays.sort(tablica2);
		Date czasKoncArrays = new Date();
		long czasArrays = czasKoncArrays.getTime() - czasStartArrays.getTime();
		
		//Porownujemy ktora metoda jest szybsza
		int szybsze; //1 - bubble, 2 - arrays, 0 - takie same
		if(czasArrays==czasBubble)
			szybsze = 0;
		else if(czasArrays>czasBubble)
			szybsze = 1;
		else
			szybsze = 2;
		
		//Spodziewamy sie ze dla tak malej liczby elementow nie ma roznicy
		assertEquals(0,szybsze);
		
		//Nie mozna porownac w ten sposob
		//assertEquals(tablica,tablica2);
		
		//Tymczasowy debug output do sprawdzania czasow
		//System.out.println("Czas bubble: " + czasBubble + " Czas arrays: " + czasArrays + " Szybsze: " + szybsze);
	}
	
	@Test
	public void srednieSortowanie()
	{
		int [] tablica = Tablice.losowa(10000); //generujemy losowa tablice
		int [] tablica2 = tablica.clone(); //kopiujemy ta tablice

		//Sprawdzamy czas sortowania b�belkowego
		Date czasStartBubble = new Date();
		Tablice.sortowanie(tablica);
		Date czasKoncBubble = new Date();
		long czasBubble = czasKoncBubble.getTime() - czasStartBubble.getTime();
		
		//Sprawdzamy czas sortowania z Arrays
		Date czasStartArrays = new Date();
		Arrays.sort(tablica2);
		Date czasKoncArrays = new Date();
		long czasArrays = czasKoncArrays.getTime() - czasStartArrays.getTime();
		
		//Porownujemy ktora metoda jest szybsza
		int szybsze; //1 - bubble, 2 - arrays, 0 - takie same
		if(czasArrays==czasBubble)
			szybsze = 0;
		else if(czasArrays>czasBubble)
			szybsze = 1;
		else
			szybsze = 2;
		
		//Tym razem szybsze powinno byc sortowanie z arrays (mamy znacznie wiecej elementow)
		assertEquals(2,szybsze);
	}
	
	@Test
	public void duzeSortowanie()
	{
		int [] tablica = Tablice.losowa(100000); //generujemy losowa tablice
		int [] tablica2 = tablica.clone(); //kopiujemy ta tablice

		//Sprawdzamy czas sortowania b�belkowego
		Date czasStartBubble = new Date();
		Tablice.sortowanie(tablica);
		Date czasKoncBubble = new Date();
		long czasBubble = czasKoncBubble.getTime() - czasStartBubble.getTime();
		
		//Sprawdzamy czas sortowania z Arrays
		Date czasStartArrays = new Date();
		Arrays.sort(tablica2);
		Date czasKoncArrays = new Date();
		long czasArrays = czasKoncArrays.getTime() - czasStartArrays.getTime();
		
		//Porownujemy ktora metoda jest szybsza
		int szybsze; //1 - bubble, 2 - arrays, 0 - takie same
		if(czasArrays==czasBubble)
			szybsze = 0;
		else if(czasArrays>czasBubble)
			szybsze = 1;
		else
			szybsze = 2;
		
		//Tutaj rowniez mamy duzo wiecej elementow
		assertEquals(2,szybsze);
	}
}
