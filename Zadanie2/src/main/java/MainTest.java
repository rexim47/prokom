import static org.junit.Assert.*;

import org.junit.Test;


public class MainTest 
{
	@Test
	public void test3argumenty() {
		String [] t = {"pierwszy","drugi", "czeci"};
		try{Main.main(t);}catch(Exception e){System.err.println(e.getMessage());}
	}
	
	@Test
	public void test1argument() {
		String [] t = {"pierwszy"};
		try{Main.main(t);}catch(Exception e){System.err.println(e.getMessage());}
	}
	
	@Test
	public void test2argumenty() {
		String [] t = {"pierwszy","drugi"};
		try{Main.main(t);}catch(Exception e){System.err.println(e.getMessage());}
	}
	
	@Test
	public void test4argumenty() {
		String [] t = {"pierwszy","drugi", "czeci", "czwarty"};
		try{Main.main(t);}catch(Exception e){System.err.println(e.getMessage());}
	}
	
	@Test
	public void test5plus6()
	{
		String [] t = {"5.0","6.0","+"};
//		String [] t6 = {"abc","6.0","+"};
		double test = 0.0;
		try{test = Main.main(t);}catch(Exception e){System.err.println(e.getMessage());}
		assertEquals(11.0,test,0.0);
	}

	@Test
	public void test5minus6()
	{
		String [] t = {"5.0","6.0","-"};
		double test = 0.0;
		try{test = Main.main(t);}catch(Exception e){System.err.println(e.getMessage());}
		assertEquals(-1.0,test,0.0);
	}
	
	@Test
	public void test5razy6()
	{
		String [] t = {"5.0","6.0","*"};
		double test = 0.0;
		try{test = Main.main(t);}catch(Exception e){System.err.println(e.getMessage());}
		assertEquals(30.0,test,0.0);
	}
	
	@Test
	public void test5przez2()
	{
		String [] t = {"5.0","2.0","/"};
		double test = 0.0;
		try{test = Main.main(t);}catch(Exception e){System.err.println(e.getMessage());}
		assertEquals(2.5,test,0.0);
	}
	
	@Test
	public void test5x6()
	{
		String [] t = {"5.0","6.0","x"};
		double test = 0.0;
		try{test = Main.main(t);}catch(Exception e){System.err.println(e.getMessage());}
		assertEquals(0.0,test,0.0);
	}
	
	@Test
	public void testAbcPlus6()
	{
		String [] t = {"abc","6.0","-"};
		double test = 0.0;
		try{test = Main.main(t);}catch(Exception e){System.err.println(e.getMessage());}
		assertEquals(0.0,test,0.0);
	}
}