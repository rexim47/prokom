import static org.junit.Assert.*;

import org.junit.Test;


public class KsiazkaTelefonicznaTest 
{
	@Test
	public void testDodawanieElem()
	{
		//Utworzenie kolekcji
		KsiazkaTelefoniczna k = new KsiazkaTelefoniczna();
				
		//Utworzenie nowego wpisu
		Wpis w = new Wpis("Abc Cba 123456789");
		
		//Dodanie wpisu do kolekcji
		k.appendElement(w);
		
		//Sprawdzenie czy wpis znajduje sie w kolekcji
		assertEquals(k.getElement(0),w);
	}
	
	public void testUsuwanieElem()
	{
		//Utworzenie kolekcji
		KsiazkaTelefoniczna k = new KsiazkaTelefoniczna();
				
		//Utworzenie nowych wpisow
		Wpis w1 = new Wpis("Abc Cba 123456789");
		Wpis w2 = new Wpis("Edf Fde 234567891");
		Wpis w3 = new Wpis("Ghi Igh 345678912");
		
		//Dodanie wpisow do kolekcji
		k.appendElement(w1);
		k.appendElement(w2);
		k.appendElement(w3);
		
		//Usuniecie srodkowego elementu
		k.removeElement(1);
		
		//Sprawdzenie czy trzeci wpis przesunal sie w miejsce drugiego
		assertEquals(k.getElement(1),w3);
	}
	
	@Test
	public void testSize()
	{
		//Utworzenie kolekcji
		KsiazkaTelefoniczna k = new KsiazkaTelefoniczna();
				
		//Utworzenie nowych wpisow
		Wpis w1 = new Wpis("Abc Cba 123456789");
		Wpis w2 = new Wpis("Edf Fde 234567891");
		Wpis w3 = new Wpis("Ghi Igh 345678912");
		
		//Dodajemy 3 wpisy do kolekcji
		k.appendElement(w1);
		k.appendElement(w2);
		k.appendElement(w3);
		
		//Spodziewamy sie rozmiaru 3, poniewa� dodali�my 3 wpisy
		assertEquals(3,k.size());
	}
	
	@Test
	public void testSortNatural() //Sortowanie po imionach -> na podstawie metody compareTo z wpisu
	{
		//Utworzenie kolekcji
		KsiazkaTelefoniczna k = new KsiazkaTelefoniczna();
		
		//Utworzenie nowych wpisow
		Wpis w1 = new Wpis("Abc Cba 123456789");
		Wpis w2 = new Wpis("Edf Fde 234567891");
		Wpis w3 = new Wpis("Ghi Igh 345678912");
		Wpis w4 = new Wpis("Jkl Lkj 456789123");
		Wpis w5 = new Wpis("Mno Onm 567891234");
		Wpis w6 = new Wpis("Prs Srp 678912345");
		Wpis w7 = new Wpis("Xyz Zyx 789123456");
		Wpis w8 = new Wpis("Tuw Wut 891234567");
		Wpis w9 = new Wpis("Art Tra 912345678");
		Wpis w10 = new Wpis("Tik Kit 111222333");
		
		//Dodanie wpisow do kolekcji w jakiejs losowej kolejnosci
		k.appendElement(w4);
		k.appendElement(w7);
		k.appendElement(w1);
		k.appendElement(w8);
		k.appendElement(w5);
		k.appendElement(w10);
		k.appendElement(w9);
		k.appendElement(w3);
		k.appendElement(w6);
		k.appendElement(w2);
		
		k.sort();
		
		//Spodziewamy sie ze wpis 1 bedzie elementem o indeksie 0 (pierwszym)
		assertEquals(k.getElement(0),w1);
		//a w7 elementem o indeksie 9 (ostatnim)
		assertEquals(k.getElement(9),w7);
	}
	
	public void testSortNazw() //Sortowanie po nazwiskach z uzyciem komparatora
	{
		//Utworzenie kolekcji
		KsiazkaTelefoniczna k = new KsiazkaTelefoniczna();
		
		//Utworzenie nowych wpisow
		Wpis w1 = new Wpis("Abc Cba 123456789");
		Wpis w2 = new Wpis("Edf Fde 234567891");
		Wpis w3 = new Wpis("Ghi Igh 345678912");
		Wpis w4 = new Wpis("Jkl Lkj 456789123");
		Wpis w5 = new Wpis("Mno Onm 567891234");
		Wpis w6 = new Wpis("Prs Srp 678912345");
		Wpis w7 = new Wpis("Xyz Zyx 789123456");
		Wpis w8 = new Wpis("Tuw Wut 891234567");
		Wpis w9 = new Wpis("Art Tra 912345678");
		Wpis w10 = new Wpis("Tik Kit 111222333");
		
		//Dodanie wpisow do kolekcji w jakiejs losowej kolejnosci
		k.appendElement(w4);
		k.appendElement(w7);
		k.appendElement(w1);
		k.appendElement(w8);
		k.appendElement(w5);
		k.appendElement(w10);
		k.appendElement(w9);
		k.appendElement(w3);
		k.appendElement(w6);
		k.appendElement(w2);
		
		k.sort(new KomparatorNazw());
		
		//Spodziewamy sie ze wpis 1 bedzie elementem o indeksie 0 (pierwszym)
		assertEquals(k.getElement(0),w1);
		//a w7 elementem o indeksie 9 (ostatnim)
		assertEquals(k.getElement(9),w7);
	}
	
	public void testSortNum() //Sortowanie po numerze z uzyciem komparatora
	{
		//Utworzenie kolekcji
		KsiazkaTelefoniczna k = new KsiazkaTelefoniczna();
		
		//Utworzenie nowych wpisow
		Wpis w1 = new Wpis("Abc Cba 123456789");
		Wpis w2 = new Wpis("Edf Fde 234567891");
		Wpis w3 = new Wpis("Ghi Igh 345678912");
		Wpis w4 = new Wpis("Jkl Lkj 456789123");
		Wpis w5 = new Wpis("Mno Onm 567891234");
		Wpis w6 = new Wpis("Prs Srp 678912345");
		Wpis w7 = new Wpis("Xyz Zyx 789123456");
		Wpis w8 = new Wpis("Tuw Wut 891234567");
		Wpis w9 = new Wpis("Art Tra 912345678");
		Wpis w10 = new Wpis("Tik Kit 111222333");
		
		//Dodanie wpisow do kolekcji w jakiejs losowej kolejnosci
		k.appendElement(w4);
		k.appendElement(w7);
		k.appendElement(w1);
		k.appendElement(w8);
		k.appendElement(w5);
		k.appendElement(w10);
		k.appendElement(w9);
		k.appendElement(w3);
		k.appendElement(w6);
		k.appendElement(w2);
		
		k.sort(new KomparatorNum());
		
		//Spodziewamy sie ze wpis 10 bedzie elementem o indeksie 0 (pierwszym)
		assertEquals(k.getElement(0),w10);
		//a w7 elementem o indeksie 9 (ostatnim)
		assertEquals(k.getElement(9),w7);
	}
	
	@Test
	public void testToString() 
	{
		//Utworzenie kolekcji
		KsiazkaTelefoniczna k = new KsiazkaTelefoniczna();
		
		//Utworzenie nowych wpisow
		Wpis w1 = new Wpis("Abc Cba 123456789");
		Wpis w2 = new Wpis("Edf Fde 234567891");
		Wpis w3 = new Wpis("Ghi Igh 345678912");
		Wpis w4 = new Wpis("Jkl Lkj 456789123");
		Wpis w5 = new Wpis("Mno Onm 567891234");
		Wpis w6 = new Wpis("Prs Srp 678912345");
		Wpis w7 = new Wpis("Xyz Zyx 789123456");
		Wpis w8 = new Wpis("Tuw Wut 891234567");
		Wpis w9 = new Wpis("Art Tra 912345678");
		Wpis w10 = new Wpis("Tik Kit 111222333");
		
		//Dodanie wpisow do kolekcji
		k.appendElement(w1);
		k.appendElement(w2);
		k.appendElement(w3);
		k.appendElement(w4);
		k.appendElement(w5);
		k.appendElement(w6);
		k.appendElement(w7);
		k.appendElement(w8);
		k.appendElement(w9);
		k.appendElement(w10);
		
		//Wypisanie wszystkich elementow w kolekcji
		System.out.println("WSZYSTKIE ELEMENTY W KOLEKCJI");
		System.out.println(k.toString());
	}

}
