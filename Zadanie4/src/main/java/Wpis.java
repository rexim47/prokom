import org.apache.commons.lang3.builder.ToStringBuilder;

public class Wpis implements Comparable<Wpis>
{
	private String imie;
	private String nazwisko;
	private String numer;
	
	//Konstruktory
	public Wpis()
	{
		imie = "";
		nazwisko = "";
		numer = "";
	}
	
	public Wpis(String _imie, String _nazwisko, String _numer)
	{
		imie = _imie;
		nazwisko = _nazwisko;
		numer = _numer;
	}
	
	public Wpis(String tekst)
	{
		imie = "";
		nazwisko = "";
		numer = "";
		int licznik=0;
		while(tekst.charAt(licznik)!=' ')
		{
			imie+=tekst.charAt(licznik);
			licznik++;
		}
		licznik++;
		while(tekst.charAt(licznik)!=' ')
		{
			nazwisko+=tekst.charAt(licznik);
			licznik++;
		}
		licznik++;
		while(licznik<tekst.length())
		{
			numer+=tekst.charAt(licznik);
			licznik++;
		}
	}
	
	//toString()
	public String toString()
	{
		//return "Imie i nazwisko: " + imie + " "+ nazwisko + "\t Nr tel.: " + numer;
		return new ToStringBuilder(this).
				append("Imie", imie).
				append("Nazwisko", nazwisko).
				append("Nr tel.", numer).
				toString();
	}
	
	//compareTo()
	
	public int compareTo(Wpis wpis)
	{
		int porownanie = imie.compareTo(wpis.imie);
		return porownanie;
	}
	
	//akcesory
	public void setImie(String _imie)
	{
		imie=_imie;
	}
	
	public String getImie()
	{
		return imie;
	}
	
	public void setNazwisko(String _nazwisko)
	{
		nazwisko=_nazwisko;
	}
	
	public String getNazwisko()
	{
		return nazwisko;
	}
	
	public void setNrTel(String _numer)
	{
		numer=_numer;
	}
	
	public String getNrTel()
	{
		return numer;
	}
	
}
