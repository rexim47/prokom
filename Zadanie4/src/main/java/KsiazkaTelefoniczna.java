
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class KsiazkaTelefoniczna
{
	private ArrayList <Wpis> wpisy = new ArrayList <Wpis>();
	
	public void appendElement(Wpis w)
	{
		wpisy.add(w);
	}
	
	public void removeElement(int id)
	{
		wpisy.remove(id);
	}
	
	public void sort()
	{
		Collections.sort(wpisy);
	}
	
	public void sort(Komparator c)
	{
		Collections.sort(wpisy, c);
	}
	
	public Wpis getElement(int pos)
	{
		return wpisy.get(pos);
	}
	
	public int size()
	{
		return wpisy.size();
	}
	
	public String toString()
	{
		String tekst="";
		for(int i=0; i<wpisy.size(); i++)
			tekst+=wpisy.get(i).toString()+"\n";
		return tekst;
	}
}
