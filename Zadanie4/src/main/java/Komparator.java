import java.util.Comparator;

public interface Komparator extends Comparator <Wpis>{
	public int compare(Wpis w1, Wpis w2);
}
