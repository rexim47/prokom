import java.util.Comparator;


public class KomparatorNum implements Komparator
{
	public int compare(Wpis w1, Wpis w2)
	{
		int poNumerze = w1.getNrTel().compareTo(w2.getNrTel());
		if(poNumerze == 0)
			return w1.compareTo(w2);
		else
		{
			return poNumerze;
		}
	}
}
