import java.util.ArrayList;


public class Lodowka {
	private ArrayList <Jedzenie> jedzenia = new ArrayList();
	private double rozmiarMax;
	private double zajeteMiejsce;
	
	public Lodowka(double rozmiar)
	{
		this.rozmiarMax=rozmiar;
		this.zajeteMiejsce=0;
	}
	
	public void wlozJedzenie(Jedzenie jedzenie)
	{
		if((getWolneMiejsce())>jedzenie.getRozmiar())
		{
			jedzenia.add(jedzenie);
			zajeteMiejsce+=jedzenie.getRozmiar();
		}
	}
	
	public void wyjmijJedzenie(int id)
	{
		if(jedzenia.size()!=0)
		{
			zajeteMiejsce-=jedzenia.get(id).getRozmiar();
			jedzenia.remove(id);
		}
	}
	
	public double getWolneMiejsce()
	{
		return rozmiarMax-zajeteMiejsce;
	}
	
	public double getZajeteMiejsce()
	{
		return zajeteMiejsce;
	}
}
