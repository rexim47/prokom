import static org.junit.Assert.*;

import org.junit.Test;


public class LodowkaTest {

	@Test
	public void test1() {
		//tworzenie lodowki
		Lodowka lodowka = new Lodowka(10);
		
		Jajka jajo = new Jajka();
		Mieso mieso = new Mieso();
		Mleko mleko = new Mleko();
		//wlozenie kazdego z jedzen
		lodowka.wlozJedzenie(jajo);
		lodowka.wlozJedzenie(mieso);
		lodowka.wlozJedzenie(mleko);
		
		//Sprawdzenie ile miejsca zajelo jedzenie
		System.out.println(lodowka.getWolneMiejsce());
		System.out.println(lodowka.getZajeteMiejsce());
		assertEquals(4, lodowka.getWolneMiejsce(),0);
		assertEquals(6, lodowka.getZajeteMiejsce(),0);
		
		//proba wlozenia mleka wiekszego od lodowki- nie udalo sie wlozyc mleka
		Lodowka pusta = new Lodowka(10);
		Mleko duze= new Mleko(11);
		pusta.wlozJedzenie(duze);
		assertEquals(0, pusta.getZajeteMiejsce(),0);
	}
	
	@Test
	public void test2() {
		//tworzenie lodowki
		Lodowka lodowka = new Lodowka(10);
		//tworzenie jedzenia
		Jajka jajo = new Jajka();
		Mieso mieso = new Mieso();
		Mleko mleko = new Mleko();
		//wlozenie do lodowki
		lodowka.wlozJedzenie(jajo);
		lodowka.wlozJedzenie(mieso);
		lodowka.wlozJedzenie(mleko);
		//sprawdzenie zgodnosci wolnego i zajetego miejsca
		assertEquals(4, lodowka.getWolneMiejsce(),0);
		assertEquals(6, lodowka.getZajeteMiejsce(),0);
		//usuniecie dowolnego jedzenia
		lodowka.wyjmijJedzenie(2);
		//ponowne sprawdzenie zgodnosci wolnego i zajetego miejsca
		assertEquals(7, lodowka.getWolneMiejsce(),0);
		assertEquals(3, lodowka.getZajeteMiejsce(),0);
	}

}
