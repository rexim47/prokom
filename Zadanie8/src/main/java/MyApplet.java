
import java.applet.Applet;
import java.awt.*; 
import java.awt.event.*; 

import javax.swing.*; 

public class MyApplet extends Applet implements ActionListener, ItemListener { // ew. extends JApplet jesli wybrano pakiet javax.swing 
	
	public void paint (Graphics g) { 
		
		//Ustawienie wymiarow appletu
		setSize(500,500);
		//Rysowanie trojkata
		g.setColor(Color.GREEN);
		g.drawLine(100, 400, 250, 100);
		g.setColor(Color.YELLOW);
		g.drawLine(250, 100, 400, 400);
		g.setColor(Color.BLUE);
		g.drawLine(100, 400, 400, 400);
		//Rysowanie prostokata
		g.setColor(Color.CYAN);
		g.drawRect(100,70,200,100);
		//Rysowanie zaaokraglonego prostokata
		g.setColor(Color.MAGENTA);
		g.drawRoundRect(110, 80, 180, 80, 30, 30);
		//Rysowanie elipsy
		g.setColor(Color.RED);
		g.drawOval(150, 95, 75, 35);
		//Wyswietlenie obrazka
		Image image = getImage(getCodeBase(), "welcome.jpg");
		g.drawImage(image, 210, 180, this);
		//Wyswietlenie napisu pogrubionym arialem 28
		String text = new String ("pogrubiona czcionka Arial 28pt.");
		g.setColor(Color.GREEN);
		Font font = new Font("Arial", Font.BOLD, 28);
		g.setFont(font);
		g.drawString(text, 50, 350);
 } 
 
 public void actionPerformed(ActionEvent event) {
	 
	    if(event.getSource() == button1){
	        fullmenuWindow.setVisible(true);
		}
		if(event.getSource() == button2){
			fullmenuWindow.setVisible(false);
		}
 }
 
 	//Deklaracje obiektow - zmiennych reprezentujacych przyciski i menuFrame
	Button 	button1, 
			button2; 
	menuFrame fullmenuWindow; 
	
	//CHECKBOXY
	Checkbox checkbox1, checkbox2;
    TextField text1;
    
    //RADIOPRZYCISKI
    CheckboxGroup checkboxgroup1;
    Checkbox radio1, radio2;
    
    //CHOICE-BOX
    Choice choice1;
	
public void init(){
	
	//Przyciski do wyswietlania/ukrywania okna menu
	button1 = new Button("Pokaz okno menu");
	add(button1);
	button1.addActionListener(this);
	button2 = new Button("Ukryj okno menu");
	add(button2);
	button2.addActionListener(this);
	
	//Okno menu
	fullmenuWindow = new menuFrame("Menu");
	fullmenuWindow.setSize(100, 100);
		
		
	//CHECKBOXY
	checkbox1 = new Checkbox("1");
	add(checkbox1);
	checkbox1.addItemListener(this);
	
	checkbox2 = new Checkbox("2");
	add(checkbox2);
	checkbox2.addItemListener(this);
        
	//RADIOPRZYCISKI
	checkboxgroup1 = new CheckboxGroup();
	radio1 = new Checkbox("Radio1", checkboxgroup1, true);
	add(radio1);
	radio1.addItemListener(this);
	
	radio2 = new Checkbox("Radio2", checkboxgroup1, false);
	add(radio2);
	radio2.addItemListener(this);
        
    //POLE WYBORU
    choice1 = new Choice();
    choice1.add("Pozycja 1");
    choice1.add("Pozycja 2");
    choice1.add("Pozycja 3");
//        choice1.add(getParameter("selection2"));
//        choice1.add(getParameter("selection3"));
//        choice1.add(getParameter("selection4"));
    add(choice1); 
    choice1.addItemListener(this);

    //POLE TEKSTOWE
    text1 = new TextField(20);
    add(text1);
        
		
	}

public void itemStateChanged(ItemEvent e) {
    if(e.getItemSelectable() == checkbox1){
            text1.setText("Kliknieto checkbox 1!");
    }

    if(e.getItemSelectable() == checkbox2){
            text1.setText("Kliknieto checkbox 2!");
    }

    if(e.getItemSelectable() == radio1){
            text1.setText("Kliknieto radioprzycisk 1!");
    }

    if(e.getItemSelectable() == radio2){
            text1.setText("Kliknieto radioprzycisk 2!");
    }
    
    if(e.getItemSelectable() == choice1){
        text1.setText(((Choice)e.getItemSelectable()).getSelectedItem());
    }

	} 
}

class menuFrame extends Frame implements ActionListener {

    Menu Menu1, SubMenu1;                                                  
    MenuBar Menubar1;                                                      
    TextField text1;                                                 
    MenuItem menuitem1, menuitem2, menuitem4;                              
    CheckboxMenuItem menuitem3;                                            
                                                                                                                                  
    menuFrame(String title){                                               
            super(title);                                                  
            text1 = new TextField("Menu");                      
            setLayout(new GridLayout(1, 1));                               
            add(text1);                                              
            Menubar1 = new MenuBar();                                      
            Menu1 = new Menu("Plik");                                      
                                                                                                                                  
            menuitem1 = new MenuItem("Wybor 1");                            
            menuitem1.addActionListener(this);                             
            Menu1.add(menuitem1);                                          
                                                                                                                                  
            menuitem2 = new MenuItem("Wybor 2");                            
            menuitem2.addActionListener(this);                             
            Menu1.add(menuitem2);                                          
                                                                                                                                  
            Menu1.addSeparator();                                          
                                                                                                                                  
            menuitem3 = new CheckboxMenuItem("Zaznacz");                
            menuitem3.addActionListener(this);                             
            Menu1.add(menuitem3);                                          
                                                                                                                                  
            Menu1.addSeparator();                                          
                                                                                                                                  
            SubMenu1 = new Menu("Podmenu");                              
            SubMenu1.add(new MenuItem("pierwsze"));                           
            SubMenu1.add(new MenuItem("drugie"));                            
            SubMenu1.add(new MenuItem("trzecie"));                            
                                                                                                                                  
            Menu1.add(SubMenu1);                                           
            Menubar1.add(Menu1);                                           
            setMenuBar(Menubar1);                                          
                                                                                                                                  
            Menu1.addSeparator();                                          
                                                                                                                                  
            menuitem4 = new MenuItem("Wyjscie");                              
            menuitem4.addActionListener(this);                             
            Menu1.add(menuitem4);                                          
    }                                                                      
                                                                                                                                  
    public void actionPerformed(ActionEvent event){                        
        if(event.getSource() == menuitem1){                                
              text1.setText("Item 1");                               
        }                                                                  
        if(event.getSource() == menuitem2){                                
              menuitem2.setEnabled(false);                                 
              text1.setText("Item 2");                               
        }                                                                  
        if(event.getSource() == menuitem3){                                
              ((CheckboxMenuItem)event.getSource()).setState(true);        
        }                                                                  
        if(event.getSource() == menuitem4){                                
              setVisible(false);                                                      
        }                                                                  
                                                                           
    }                                                              
}
