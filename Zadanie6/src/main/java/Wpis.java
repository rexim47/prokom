import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Wpis implements Comparable<Wpis>, Serializable, Cloneable
{
	private String imie;
	private String nazwisko;
	private String numer;
	
	//Konstruktory
	public Wpis()
	{
		imie = "";
		nazwisko = "";
		numer = "";
	}
	
	public Wpis(String _imie, String _nazwisko, String _numer)
	{
		imie = _imie;
		nazwisko = _nazwisko;
		numer = _numer;
	}
	
	public Wpis(String tekst)
	{
		imie = "";
		nazwisko = "";
		numer = "";
		int licznik=0;
		while(tekst.charAt(licznik)!=' ')
		{
			imie+=tekst.charAt(licznik);
			licznik++;
		}
		licznik++;
		while(tekst.charAt(licznik)!=' ')
		{
			nazwisko+=tekst.charAt(licznik);
			licznik++;
		}
		licznik++;
		while(licznik<tekst.length())
		{
			numer+=tekst.charAt(licznik);
			licznik++;
		}
	}
	
	//toString()
	public String toString()
	{
		//return "Imie i nazwisko: " + imie + " "+ nazwisko + "\t Nr tel.: " + numer;
		return new ToStringBuilder(this).
				append("Imie", imie).
				append("Nazwisko", nazwisko).
				append("Nr tel.", numer).
				toString();
	}
	
	//compareTo()
	
	public int compareTo(Wpis wpis)
	{
		int porownanie = imie.compareTo(wpis.imie);
		if(porownanie == 0)
		{
			porownanie = nazwisko.compareTo(wpis.nazwisko);
			if(porownanie == 0)
				porownanie = numer.compareTo(wpis.numer);
		}
		return porownanie;
	}
	
	//akcesory
	public void setImie(String _imie)
	{
		imie=_imie;
	}
	
	public String getImie()
	{
		return imie;
	}
	
	public void setNazwisko(String _nazwisko)
	{
		nazwisko=_nazwisko;
	}
	
	public String getNazwisko()
	{
		return nazwisko;
	}
	
	public void setNrTel(String _numer)
	{
		numer=_numer;
	}
	
	public String getNrTel()
	{
		return numer;
	}
	
	@Override
    public Object clone() throws CloneNotSupportedException {
        //return super.clone();
		Wpis w = new Wpis();
		String _imie = this.imie;
		String _nazwisko = this.nazwisko;
		String _numer = this.numer;
		w.setImie(_imie);
		w.setNazwisko(_nazwisko);
		w.setNrTel(_numer);
		return w;
    }
}
