import static org.junit.Assert.*;

import org.junit.Test;


public class KsiazkaTelefonicznaTest 
{
	@Test
	public void test() 
	{
		//Utworzenie kolekcji
		KsiazkaTelefoniczna k = new KsiazkaTelefoniczna();
		
		//Utworzenie nowych wpisow
		Wpis w1 = new Wpis("Abc Cba 123456789");
		Wpis w2 = new Wpis("Edf Fde 234567891");
		Wpis w3 = new Wpis("Ghi Igh 345678912");
		Wpis w4 = new Wpis("Jkl Lkj 456789123");
		Wpis w5 = new Wpis("Mno Onm 567891234");
		Wpis w6 
		= new Wpis("Prs Srp 678912345");
		Wpis w7 = new Wpis("Xyz Zyx 789123456");
		Wpis w8 = new Wpis("Tuw Wut 891234567");
		Wpis w9 = new Wpis("Art Tra 912345678");
		Wpis w10 = new Wpis("Tik Kit 111222333");
		
		//Dodanie wpisow do kolekcji
		k.appendElement(w1);
		k.appendElement(w2);
		k.appendElement(w3);
		k.appendElement(w4);
		k.appendElement(w5);
		k.appendElement(w6);
		k.appendElement(w7);
		k.appendElement(w8);
		k.appendElement(w9);
		k.appendElement(w10);
		
		//Wypisanie wszystkich elementow w kolekcji
		System.out.println("WSZYSTKIE ELEMENTY W KOLEKCJI");
		System.out.println(k.toString());
		System.out.print("ILOSC ELEMENTOW W KOLEKCJI: ");
		System.out.println(k.size());
		System.out.println();
		
		//Usuniecie 2 elementow
		k.removeElement(2);
		k.removeElement(1);
		//Wypisanie kolekcji po usuwaniu
		System.out.println("ELEMENTY KOLEKCJI PO USUNIECIU 2 WPISOW");
		System.out.println(k.toString());
		System.out.print("ILOSC ELEMENTOW W KOLEKCJI: ");
		System.out.println(k.size());
		System.out.println();
		
		//Sortowanie wzgledem porzadku naturalnego
		k.sort();
		System.out.println("ELEMENT KOLEKCJI POSORTOWANE PORZADKIEM NATURALNYM");
		System.out.println(k.toString());
		
		//Sortowanie po nazwiskach, nastepnie po imionach
		k.sort(new KomparatorNazw());
		System.out.println("ELEMENTY KOLEKCJI POSORTOWANE PO NAZWISKU, A POZNIEJ PO IMIENIU");
		System.out.println(k.toString());
		
		//Sortowanie po numerze telefonu
		k.sort(new KomparatorNum());
		System.out.println("ELEMENTY KOLEKCJI POSORTOWANE PO NUMERZE TELEFONU");
		System.out.println(k.toString());
	}

}
