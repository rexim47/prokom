import static org.junit.Assert.*;

import org.junit.Test;


public class WpisTest {

	@Test
	public void test()
	{
		Wpis wpis1= new Wpis("Adam","Nowak", "1235");
		Wpis wpis2= new Wpis("Adam Nowak 123456");
		System.out.println(wpis1.toString());
		System.out.println(wpis2.toString());
		
		Wpis [] wpisy = new Wpis[10];
		wpisy[0]=new Wpis("Adam Nowak 123456789");
		wpisy[1]=new Wpis("Adam Newak 1234");
		wpisy[2]=new Wpis("Adam Nowakowski 6789");
		wpisy[3]=new Wpis("Adam Nowacki 89");
		wpisy[4]=new Wpis("Adam Nowicki 13468");
		wpisy[5]=new Wpis("Adam Nowy 1357");
		wpisy[6]=new Wpis("Sadam Nowak 246");
		wpisy[7]=new Wpis("Sadam Nowakowski 11315189");
		wpisy[8]=new Wpis("Jack Nowack 997");
		wpisy[9]=new Wpis("Sadam Nowacki 9111922");
		
		for(int i=0; i<10; i++)
			System.out.println(wpisy[i].toString());
		
	}

}
