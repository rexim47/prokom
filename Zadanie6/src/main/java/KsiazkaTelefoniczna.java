import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Logger;


public class KsiazkaTelefoniczna implements Serializable, Cloneable
{
	private ArrayList <Wpis> wpisy = new ArrayList();
	
	public ArrayList getWpisy()
	{
		return wpisy;
	}
	
	public void setWpisy(ArrayList <Wpis> wpisy)
	{
		this.wpisy = wpisy;
	}
	
	public void appendElement(Wpis w)
	{
		wpisy.add(w);
	}
	
	public void removeElement(int id)
	{
		wpisy.remove(id);
	}
	
	public void sort()
	{
		Collections.sort(wpisy);
	}
	
	public void sort(Komparator c)
	{
		Collections.sort(wpisy, c);
	}
	
	public Wpis getElement(int pos)
	{
		try{
		return wpisy.get(pos);
		}
		catch(IndexOutOfBoundsException ob){
			TypeRuntimeException tre= new TypeRuntimeException(ob.getMessage());
			throw tre;
		}
	}
	
	public int size()
	{
		return wpisy.size();
	}
	
	public String toString()
	{
		String tekst="";
		for(int i=0; i<wpisy.size(); i++)
			tekst+=wpisy.get(i).toString()+"\n";
		return tekst;
	}
	
	@Override
    public Object clone() throws CloneNotSupportedException {
        //return super.clone();
		KsiazkaTelefoniczna k = new KsiazkaTelefoniczna();
		ArrayList <Wpis> _wpisy = (ArrayList) this.getWpisy().clone();
		for(int i=0;i<this.wpisy.size();i++)
			_wpisy.set(i,((Wpis)this.wpisy.get(i).clone()));
		k.setWpisy(_wpisy);
		return k;
    }
}
