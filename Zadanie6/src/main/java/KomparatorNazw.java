import java.util.Comparator;

public class KomparatorNazw implements Komparator
{
	public int compare(Wpis w1, Wpis w2)
	{
		int poNazwiskach = w1.getNazwisko().compareTo(w2.getNazwisko());
		if(poNazwiskach == 0)
			return w1.compareTo(w2);
		else
		{
			return poNazwiskach;
		}
	}
}
