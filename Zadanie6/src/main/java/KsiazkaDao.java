import java.io.File;


public interface KsiazkaDao {
	public void zapis(KsiazkaTelefoniczna ksiazkaTel, File plik) throws TypeException;
	public KsiazkaTelefoniczna odczyt(File plik) throws TypeException;
}
