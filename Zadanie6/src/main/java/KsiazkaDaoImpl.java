import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.logging.Logger;


public class KsiazkaDaoImpl implements KsiazkaDao {

	private final static Logger log = Logger.getLogger(KsiazkaDaoImpl.class.getName());
	
	public void zapis(KsiazkaTelefoniczna ksiazkaTel, File plik) throws TypeException{
		try 
		{ 
			plik.createNewFile();
			ObjectOutputStream strumienZapisu = new ObjectOutputStream(new FileOutputStream(plik));
			try{
				strumienZapisu.writeObject(ksiazkaTel);
			} finally{
				if (strumienZapisu != null)
				{
					strumienZapisu.close();
				}			
			}
			//strumienZapisu.close();
		} // Instrukcje lapiace wyjatki 
		catch (IOException io) {
			//log.warning(io.getMessage());
			TypeException te = new TypeException(io.getMessage());
			throw te;
			//try{throw te;} 
			//catch (TypeException e) {log.warning(te.getMessage());}
			} 
		catch (Exception se) {
			TypeException te = new TypeException(se.getMessage());
			throw te;
		}
	}
	
	public KsiazkaTelefoniczna odczyt(File plik) throws TypeException {
		KsiazkaTelefoniczna ksiazkaTel = null;
		ObjectInputStream strumienOdczytu = null;
		try
		{	
			strumienOdczytu = new ObjectInputStream(new FileInputStream(plik));
			try{
				//strumienOdczytu.readObject();
			} finally{
				if (strumienOdczytu != null)
				{
					ksiazkaTel = (KsiazkaTelefoniczna)strumienOdczytu.readObject();
					strumienOdczytu.close();
				}
			}
			//ksiazkaTel = (KsiazkaTelefoniczna)strumienOdczytu.readObject();
			//strumienOdczytu.close();
		}
		catch (FileNotFoundException io) {
//			log.warning(io.getMessage());
//			System.out.println("FILE NOT FOUND!");
			//System.out.println(io.getMessage());
			
			TypeException te = new TypeException(io.getMessage());
			throw te;
		}
		catch (IOException io) {
//			log.warning(io.getMessage());
//			System.out.println("IO EXCEPTION!");
			//System.out.println(io.getMessage());
			TypeException te = new TypeException(io.getMessage());
			throw te;
		} 
		
		catch (ClassNotFoundException e) {
			//e.printStackTrace();
			System.out.println("CLASS NOT FOUND!");
			log.warning(e.getMessage());
		}
		return ksiazkaTel;
	}
}
