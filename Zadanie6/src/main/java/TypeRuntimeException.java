
public class TypeRuntimeException extends RuntimeException {
	public TypeRuntimeException() {
    }

    public TypeRuntimeException(String message) {
        super(message);
    }

    public TypeRuntimeException(Throwable cause) {
        super(cause);
    }

    public TypeRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
