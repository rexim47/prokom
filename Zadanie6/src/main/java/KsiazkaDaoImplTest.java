import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;


public class KsiazkaDaoImplTest {

	@Test
	public void test() {
		//Utworzenie kolekcji
		KsiazkaTelefoniczna k = new KsiazkaTelefoniczna();
		
		//Utworzenie nowych wpisow
		Wpis w1 = new Wpis("Abc Cba 123456789");
		Wpis w2 = new Wpis("Edf Fde 234567891");
		Wpis w3 = new Wpis("Ghi Igh 345678912");
		Wpis w4 = new Wpis("Jkl Lkj 456789123");
		Wpis w5 = new Wpis("Mno Onm 567891234");
		Wpis w6 = new Wpis("Prs Srp 678912345");
		Wpis w7 = new Wpis("Xyz Zyx 789123456");
		Wpis w8 = new Wpis("Tuw Wut 891234567");
		Wpis w9 = new Wpis("Art Tra 912345678");
		Wpis w10 = new Wpis("Tik Kit 111222333");
		
		//Dodanie wpisow do kolekcji
		k.appendElement(w1);
		k.appendElement(w2);
		k.appendElement(w3);
		k.appendElement(w4);
		k.appendElement(w5);
		k.appendElement(w6);
		k.appendElement(w7);
		k.appendElement(w8);
		k.appendElement(w9);
		k.appendElement(w10);
		
		KsiazkaDaoImpl kdao = new KsiazkaDaoImpl();
		
		File plik = new File("serjalizacja.txt");
		
		try {
			kdao.zapis(k,plik);
		} catch (TypeException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
		KsiazkaTelefoniczna k2 = null;
		try {
			k2 = kdao.odczyt(plik);
		} catch (TypeException e) {
			System.out.println(e.getMessage());
		}
		for(int i=0;i<k.size();i++)
			assertEquals(0,k.getElement(i).compareTo(k2.getElement(i)));
		System.out.println("Rozmiar: "  + k2.size());
		System.out.println(k2.toString());
	}

	@Test
	public void testKlonowanie() {
		
		KsiazkaTelefoniczna k1 = new KsiazkaTelefoniczna();
		
		Wpis w1 = new Wpis("Abc Cba 123456789");
		k1.appendElement(w1);
		
		KsiazkaTelefoniczna k2;
		try {
			k2 = (KsiazkaTelefoniczna)k1.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			k2 = null;
		}
		
		//Porownanie obiektu oryginalnego i zklonowanego
		System.out.print("Obiekt bazowy: " + k1.toString());
		System.out.print("Obiekt klon: " + k2.toString());
				
		//Obiekt zklonowany zawiera te same dane co obiekt kopiowany
		assertEquals(0,k1.getElement(0).compareTo(k2.getElement(0)));
		//Ale nie sa to te same obiekty
		assertEquals(false,k1.equals(k2));
		//Zmieniamy dane w 1 obiekcie
		k1.getElement(0).setImie("ZmienioneImie");
		//Sprawdzamy czy nadal oba obiekty zawieraja te same dane (nie powinny)
		assertNotEquals(0,k1.getElement(0).compareTo(k2.getElement(0)));
		
		//Po zmianie
		System.out.print("Obiekt bazowy po zmianie: " + k1.toString());
		System.out.print("Obiekt klon po zmianie: " + k2.toString());
		//assertEquals(k1,k2);
	}
	
	@Test
	public void testRuntimeException()
	{
		boolean flag = false; // czy wystapil wyjatek
		KsiazkaTelefoniczna k = new KsiazkaTelefoniczna();
		try
		{
			k.getElement(100);
		}
		catch(TypeRuntimeException e)
		{
			System.out.println(e.getMessage());
			flag = true; // wystapil
		}
		assertTrue(flag);
	}
	
	@Test
	public void testException()
	{
		boolean flag = false; // czy wystapil wyjatek
		
		KsiazkaTelefoniczna k = new KsiazkaTelefoniczna();
		
		Wpis w1 = new Wpis("Abc Cba 123456789");
		Wpis w2 = new Wpis("Edf Fde 234567891");
		Wpis w3 = new Wpis("Ghi Igh 345678912");
		
		k.appendElement(w1);
		k.appendElement(w2);
		k.appendElement(w3);
		
		KsiazkaDaoImpl kdao = new KsiazkaDaoImpl();
		File plik = new File("error.txt");
		plik.setReadOnly();
		try
		{
			kdao.zapis(k, plik);
		}
		catch(TypeException te)
		{
			System.out.println(te.getMessage());
			flag = true; // wystapil
		}
		assertTrue(flag);
	}
}
