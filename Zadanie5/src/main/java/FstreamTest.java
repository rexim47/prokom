import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.Test;


public class FstreamTest {

	Fstream f = new Fstream();
	
	@Test
	public void testZapisTxt() {
		String s="fdakfdklsanflnsa�����󳳳������";
		File file = new File("test.txt");
		f.zapisTxt("fdakfdklsanflnsa�����󳳳������", file);
		assertEquals(s,f.odczytTxt(file));
	}
	
	@Test
	public void testOdczytTxt() {
		File file = new File("test.txt");
		System.out.println(f.odczytTxt(file));
		String s = "fdakfdklsanflnsa�����󳳳������";
		String test = f.odczytTxt(file);
		assertEquals(s,test);
	}
	
	@Test
	public void testZapisTab() {
		int tab[] = {0,0,2,6,12,20,30,42,56,72};
		File file = new File("test1.txt");
		f.zapisTab(file,Tablice.dynamiczna(10));
		assertArrayEquals(tab,f.odczytTab(file));
	}
	
	@Test
	public void testOdczytTab(){
		File file = new File("test1.txt");
		Tablice.drukujTablice(System.out, f.odczytTab(file));
		int tab[] = {0,0,2,6,12,20,30,42,56,72};
		int[] test = f.odczytTab(file);
		assertArrayEquals(tab,test);
	}
}
