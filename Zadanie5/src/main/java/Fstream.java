import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.logging.*;

public class Fstream
{
	private final static Logger log = Logger.getLogger(Fstream.class.getName());
	public void zapisTxt(String tekst, File plik)
	{
		try 
		{
			plik.createNewFile(); // Utworzenie pliku pod sciezka zapisana w plik
			OutputStreamWriter strumienZapisu = new OutputStreamWriter(
				     new FileOutputStream(plik),
				     Charset.forName("UTF-8").newEncoder() 
				 ); 
			strumienZapisu.write(tekst, 0, tekst.length()); // Zapis do pliku liter od 0 do 100 z txt 
			strumienZapisu.close(); // Zamkniecie strumienia 
		} // Instrukcje lapiace wyjatki 
		catch (IOException io) {log.warning(io.getMessage());} 
		catch (Exception se) {log.warning("blad sec");}
	}
	
	public String odczytTxt(File plik)
	{
		String tekst = "";
		try
		{
			FileInputStream strumienOdczytu = new FileInputStream(plik);
			byte [] dane = new byte[(int)plik.length()];
			strumienOdczytu.read(dane);
			strumienOdczytu.close();
			tekst = new String(dane, "UTF-8");
		}
		catch (FileNotFoundException io) {log.warning(io.getMessage());}
		catch (IOException io) {log.warning(io.getMessage());} 
		return tekst;
	}
	
	public void zapisTab(File plik, int [] tablica)
	{
		try 
		{ 
			 DataOutputStream strumienTablicy = new DataOutputStream(new FileOutputStream(plik));// Strumien zapisujacy liczby  
			 for (int i=0; i< tablica.length ; i++) strumienTablicy.writeInt(tablica[i]); 
			 strumienTablicy.close();
		} 
		catch (IOException io) {log.warning(io.getMessage());} 
		catch (Exception se) {log.warning("blad sec");}
	}
	
	public int[] odczytTab(File plik)
	{
		int [] tab = new int [1];
		tab[0] = 0;
		int rozmiar = 1;
		try
		{
			DataInputStream strumienTablicaZPliku = new DataInputStream(new FileInputStream(plik)); 
			while(strumienTablicaZPliku.available()>0)
			{
				if(rozmiar == 1)
					tab[0] = strumienTablicaZPliku.readInt();
				else
				{
					tab = Arrays.copyOf(tab, rozmiar);
					tab[rozmiar-1] = strumienTablicaZPliku.readInt();
				}
				rozmiar++;
			}
			strumienTablicaZPliku.close();
		} 
		catch (FileNotFoundException io) {log.warning(io.getMessage());} 
		catch (IOException io) {log.warning(io.getMessage());}
		return tab;
	}
}