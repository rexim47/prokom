package Interface;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;

import java.awt.CardLayout;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.JLabel;
import javax.swing.SpinnerNumberModel;

import Data.Event;
import Data.EventContainer;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;
import com.toedter.components.JLocaleChooser;

import javax.swing.JCheckBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Calendar;
import java.util.Date;

public class AddEvent extends JDialog {
	
	private final JPanel contentPanel = new JPanel();
	private JTextField txtTitle;
	private JTextField txtLocalization;
	private JTextField txtDescription;
	
	private EventContainer eventsRef;
	private JCalendar calendarRef;
	private Event eventToEdit;
	private Kalendarz kalendarzRef;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			AddEvent dialog = new AddEvent();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public AddEvent(JFrame owner, Kalendarz kalendarz, EventContainer events, JCalendar calendar, Event _eventToEdit) {
		super(owner, "Dodaj wydarzenie");
		setLocation(owner.getLocation().x + 100, owner.getLocation().y + 100);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setVisible(true);
		
		this.eventsRef = events;
		this.calendarRef = calendar;
		this.eventToEdit = _eventToEdit;
		this.kalendarzRef = kalendarz;
		
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			txtTitle = new JTextField();
			txtTitle.setBounds(10, 10, 86, 20);
			if(eventToEdit!=null)
				txtTitle.setText(eventToEdit.getTitle());
			else
				txtTitle.setText("Nazwa");
			txtTitle.setToolTipText("");
			contentPanel.add(txtTitle);
			txtTitle.setColumns(10);
		}
		{
			txtLocalization = new JTextField();
			txtLocalization.setBounds(10, 41, 86, 20);
			if(eventToEdit!=null)
				txtLocalization.setText(eventToEdit.getLocalization());
			else
				txtLocalization.setText("Miejsce");
			contentPanel.add(txtLocalization);
			txtLocalization.setColumns(10);
		}
		{
			txtDescription = new JTextField();
			txtDescription.setBounds(106, 10, 318, 51);
			if(eventToEdit!=null)
				txtDescription.setText(eventToEdit.getDescription());
			else
				txtDescription.setText("Opis");
			contentPanel.add(txtDescription);
			txtDescription.setColumns(10);
		}
		
		final JSpinner spinnerHour = new JSpinner();
		spinnerHour.setModel(new SpinnerNumberModel(0, 0, 24, 1));
		spinnerHour.setBounds(139, 72, 46, 20);
		contentPanel.add(spinnerHour);
		
		if(eventToEdit!=null)
			spinnerHour.getModel().setValue(eventToEdit.getDate().getHours());
		
		JLabel lblGodzina = new JLabel("Godzina:");
		lblGodzina.setBounds(63, 75, 58, 14);
		contentPanel.add(lblGodzina);
		
		final JSpinner spinnerMinutes = new JSpinner();
		spinnerMinutes.setModel(new SpinnerNumberModel(0, 0, 59, 1));
		spinnerMinutes.setBounds(251, 72, 46, 20);
		contentPanel.add(spinnerMinutes);
		
		if(eventToEdit!=null)
			spinnerMinutes.getModel().setValue(eventToEdit.getDate().getMinutes());
		
		final JSpinner spinnerReminder = new JSpinner();
		spinnerReminder.setModel(new SpinnerNumberModel(0, 0, 59, 1));
		spinnerReminder.setBounds(205, 103, 46, 20);
		contentPanel.add(spinnerReminder);
		
		if(eventToEdit!=null)
		{
			Integer value = (int) ((eventToEdit.getDate().getTime()-eventToEdit.getReminderDate().getTime())/60000);
			spinnerReminder.getModel().setValue(value);
		}
		
		JLabel lblMinutPrzed = new JLabel("minut przed.");
		lblMinutPrzed.setBounds(261, 106, 86, 14);
		contentPanel.add(lblMinutPrzed);
		
		final JCheckBox chckbxReminderCheckBox = new JCheckBox("Przypomnienie:");
		chckbxReminderCheckBox.setBounds(75, 102, 124, 23);
		contentPanel.add(chckbxReminderCheckBox);
		
		JLabel lblMinuta = new JLabel("Minuta:");
		lblMinuta.setBounds(195, 75, 46, 14);
		contentPanel.add(lblMinuta);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Ok");
				okButton.addActionListener(new ActionListener() {
					@SuppressWarnings("deprecation")
					public void actionPerformed(ActionEvent e) {
						Event event = new Event();
						if(eventToEdit!=null)
						{
							event.setId(eventToEdit.getId());
						}
						else
						{
							event.setId(eventsRef.getNextId());
						}
						event.setTitle(txtTitle.getText());
						event.setDescription(txtDescription.getText());
						event.setLocalization(txtLocalization.getText());
						event.setDate(calendarRef.getDate());
						event.getDate().setHours((Integer) spinnerHour.getValue());
						event.getDate().setMinutes((Integer) spinnerMinutes.getValue());
						long eventDateValue = event.getDate().getTime();
						Date reminderDate = new Date(eventDateValue - (((Integer) spinnerReminder.getValue())*60000));
						event.setReminderDate(reminderDate);
						if(chckbxReminderCheckBox.isSelected())
							event.setReminderStatus(true);
						if(eventToEdit!=null)
							eventsRef.replaceEvent(eventToEdit.getId(), event);
						else
							eventsRef.addEvent(event);
						kalendarzRef.updateEventsList();
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Anuluj");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
