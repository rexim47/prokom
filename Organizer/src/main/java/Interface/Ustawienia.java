package Interface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ustawienia extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JCheckBox chckbxChangeReminderStatus;
	private Kalendarz kalRef;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			Ustawienia dialog = new Ustawienia();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public Ustawienia(JFrame owner, Kalendarz kal) {
		super(owner, "Ustawienia");
		this.kalRef = kal;
		setLocation(owner.getLocation().x + 100, owner.getLocation().y + 100);
		setBounds(100, 100, 450, 300);
		setVisible(true);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			chckbxChangeReminderStatus = new JCheckBox("Powiadomienia");
			chckbxChangeReminderStatus.setSelected(kalRef.getReminderOn());
			chckbxChangeReminderStatus.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent arg0) {
					kalRef.changeReminderOn();
				}
			});
			contentPanel.add(chckbxChangeReminderStatus);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
