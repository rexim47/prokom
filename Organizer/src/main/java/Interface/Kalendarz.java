package Interface;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Data.Event;
import Data.EventContainer;
import Logic.ConverterDB;
import Logic.ConverterXML;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;

import javax.swing.DefaultListModel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JOptionPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import java.awt.event.ActionListener;

import javax.swing.JList;
import javax.swing.JSplitPane;
import javax.swing.JButton;
import javax.swing.JInternalFrame;

import java.awt.Component;

import javax.swing.Box;
import javax.swing.JTextField;

import java.awt.Button;

import javax.swing.SwingConstants;
import javax.swing.BoxLayout;

import java.awt.FlowLayout;

import javax.swing.JScrollPane;

import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.Date;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class Kalendarz extends JFrame {
	
	private static JPanel contentPane;
	private JCalendar calendar;
	private DefaultListModel eventsListModel;
	private JList eventsList;
	
	//Kontener na wydarzenia
	private static EventContainer events;
	private static Event eventRemind = null;
	private ConverterXML converterXML;
	private ConverterDB converterDB;
	
	private static boolean reminderOn = true;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Kalendarz frame = new Kalendarz();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
//		EventContainer eventsRef = new EventContainer();
//		eventsRef.setAll(events);
		while(true)
		{
			if(reminderOn)
			{
				try
				{
					eventRemind = events.selectEventWithReminder(Calendar.getInstance().getTime());
					if(eventRemind!=null)
					{
						System.out.println("Poszlo");
						JOptionPane.showMessageDialog(contentPane, "Zbliza sie wydarzenie o tytule: " + eventRemind.getTitle(),"Przypomnienie",JOptionPane.WARNING_MESSAGE);
					}
				}
				catch (NullPointerException e)
				{
					//e.printStackTrace();
				}
			}
			//checkReminder();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Create the frame.
	 */
	public Kalendarz() {
		events = new EventContainer();
		converterXML = new ConverterXML();
		converterDB = new ConverterDB();
		
		setTitle("Kalendarz");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 450);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMenu = new JMenu("Menu");
		menuBar.add(mnMenu);
		
		JMenuItem mntmSettings = new JMenuItem("Ustawienia");
		mntmSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Ustawienia(Kalendarz.this, Kalendarz.this);
				repaint();
			}
		});
		mnMenu.add(mntmSettings);
		
		JMenuItem mntmSaveToDB = new JMenuItem("Zapis do Bazy Danych");
		mntmSaveToDB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				converterDB.save(events);
			}
		});
		mnMenu.add(mntmSaveToDB);
		
		JMenuItem mntmLoadFromDB = new JMenuItem("Odczyt z Bazy Danych");
		mntmLoadFromDB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				events = converterDB.load();
			}
		});
		mnMenu.add(mntmLoadFromDB);
		
		JMenuItem mntmSaveToXML = new JMenuItem("Zapis do XML");
		mntmSaveToXML.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				converterXML.save(events);
			}
		});
		mnMenu.add(mntmSaveToXML);
		
		JMenuItem mntmLoadFromXML = new JMenuItem("Odczyt z XML");
		mntmLoadFromXML.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				events = converterXML.load();
			}
		});
		mnMenu.add(mntmLoadFromXML);
		
		JMenuItem mntmSaveToOutlook = new JMenuItem("Eksport do Outlooka");
		mnMenu.add(mntmSaveToOutlook);
		
		JMenu mnEditMenu = new JMenu("Edycja");
		menuBar.add(mnEditMenu);
		
		JMenuItem mntmAdd = new JMenuItem("Dodaj");
		mntmAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Date currentDate = new Date();
				System.out.println(calendar.getDate().getDay() + " " + currentDate.getDay());
				if(calendar.getDate().getYear()>=currentDate.getYear())
					if(calendar.getDate().getMonth()>=currentDate.getMonth())
						//Sprawdzanie po dniach nie dzia�a narazie
						if(calendar.getDate().getDay()>=currentDate.getDay())
							new AddEvent(Kalendarz.this, Kalendarz.this, events, calendar,null);
			}
		});
		mnEditMenu.add(mntmAdd);
		
		JMenuItem mntmEdytuj = new JMenuItem("Edytuj");
		mnEditMenu.add(mntmEdytuj);
		
		JMenuItem mntmRemove = new JMenuItem("Usu\u0144");
		mnEditMenu.add(mntmRemove);
		
		JMenu mnFilters = new JMenu("Filtry");
		menuBar.add(mnFilters);
		
		JMenuItem mntmFilterByTitle = new JMenuItem("Po tytule");
		mntmFilterByTitle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Filter(Kalendarz.this, Kalendarz.this, 1);
			}
		});
		mnFilters.add(mntmFilterByTitle);
		
		JMenuItem mntmFilterByLocalization = new JMenuItem("Po miejscu");
		mntmFilterByLocalization.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Filter(Kalendarz.this, Kalendarz.this, 2);
			}
		});
		mnFilters.add(mntmFilterByLocalization);
		
		JMenuItem mntmRemoveByDate = new JMenuItem("Usu\u0144 starsze ni\u017C...");
		mntmRemoveByDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Cleaner(Kalendarz.this, Kalendarz.this);
			}
		});
		mnFilters.add(mntmRemoveByDate);
		
		JMenu mnHelp = new JMenu("Pomoc");
		menuBar.add(mnHelp);
		
		JMenuItem mntmAbout = new JMenuItem("O programie");
		mnHelp.add(mntmAbout);
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(contentPane, "Organizer\nProjekt na Programowanie Komponentowe\nAutorzy:\nAdrian Majzer 189723\nJakub Pietrzak 189749","O programie",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel eventsPane = new JPanel();
		contentPane.add(eventsPane, BorderLayout.WEST);
		eventsPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JScrollPane scrollPane = new JScrollPane();
		eventsPane.add(scrollPane);
		
		eventsListModel = new DefaultListModel();
		eventsList = new JList(eventsListModel);
		eventsList.setVisibleRowCount(20);
		scrollPane.setViewportView(eventsList);
		
		JPanel calendarPane = new JPanel();
		contentPane.add(calendarPane, BorderLayout.CENTER);
		calendarPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		calendar = new JCalendar();
		calendar.getDayChooser().getDayPanel().setPreferredSize(new Dimension(280, 310));
		calendarPane.add(calendar);
		
		calendar.getDayChooser().addPropertyChangeListener(new PropertyChangeListener(){
			public void propertyChange(PropertyChangeEvent arg0) {
				updateEventsList();
			}
		});
		
		JPanel buttonsPane = new JPanel();
		contentPane.add(buttonsPane, BorderLayout.SOUTH);
		
		JButton btnAdd = new JButton("Dodaj");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Calendar cal1 = Calendar.getInstance();
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(calendar.getDate());
				//System.out.println(calendar.getDate().getDay() + " " + currentDate.getDay());
				if(cal2.get(Calendar.YEAR)>=cal1.get(Calendar.YEAR))
					if(cal2.get(Calendar.MONTH)>=cal1.get(Calendar.MONTH))
						//Sprawdzanie po dniach nie dzia�a narazie
						if(cal2.get(Calendar.DAY_OF_MONTH)>=cal1.get(Calendar.DAY_OF_MONTH))
							new AddEvent(Kalendarz.this, Kalendarz.this, events, calendar,null);
			}
		});
		
		JButton btnRefresh = new JButton("Od\u015Bwie\u017C");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateEventsList();
			}
		});
		buttonsPane.add(btnRefresh);
		buttonsPane.add(btnAdd);
		
		JButton btnEdit = new JButton("Edytuj");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(eventsList.getSelectedValue()!=null)
					new AddEvent(Kalendarz.this, Kalendarz.this, events, calendar,(Event)eventsList.getSelectedValue());
			}
		});
		buttonsPane.add(btnEdit);
		
		JButton btnRemove = new JButton("Usu\u0144");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(eventsList.getSelectedValue()!=null)
				{
					events.removeEvent((Event)eventsList.getSelectedValue());
					updateEventsList();
				}
			}
		});
		buttonsPane.add(btnRemove);
	}
	
	public void updateEventsList()
	{
		eventsListModel.clear();
		EventContainer eventsByDate = events.selectEventsByDate(calendar.getDate());
		for(int i=0;i<eventsByDate.size();i++)
			eventsListModel.addElement(eventsByDate.getEvent(i));
	}
	
	public void filterByTitle(String title)
	{
		eventsListModel.clear();
		EventContainer eventsByTitle = events.selectEventsByTitle(title);
		for(int i=0;i<eventsByTitle.size();i++)
			eventsListModel.addElement(eventsByTitle.getEvent(i));
	}
	
	public void filterByLocalization(String localization)
	{
		eventsListModel.clear();
		EventContainer eventsByLocalization = events.selectEventsByLocalization(localization);
		for(int i=0;i<eventsByLocalization.size();i++)
			eventsListModel.addElement(eventsByLocalization.getEvent(i));
	}
	
	public void cleanByDate(Date date)
	{
		events.cleanEventsByDate(date);
	}
	
	public void changeReminderOn()
	{
		reminderOn = !reminderOn;
	}
	
	public boolean getReminderOn()
	{
		return reminderOn;
	}
}
