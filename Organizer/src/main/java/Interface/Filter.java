package Interface;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Filter extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtFilteredText;
	
	private Kalendarz kalendarzRef;
	private int optionRef;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			Filter dialog = new Filter();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public Filter(JFrame owner, Kalendarz kalendarz, int option) {
		super(owner, "Filtruj");
		this.kalendarzRef = kalendarz;
		this.optionRef = option;
		setLocation(owner.getLocation().x + 100, owner.getLocation().y + 100);
		setBounds(100, 100, 350, 230);
		setVisible(true);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			txtFilteredText = new JTextField();
			txtFilteredText.setHorizontalAlignment(SwingConstants.CENTER);
			txtFilteredText.setBounds(10, 11, 314, 136);
			txtFilteredText.setText("Wpisz szukan\u0105 fraz\u0119");
			contentPanel.add(txtFilteredText);
			txtFilteredText.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Filtruj");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(optionRef==1)
							kalendarzRef.filterByTitle(txtFilteredText.getText());
						if(optionRef==2)
							kalendarzRef.filterByLocalization(txtFilteredText.getText());
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Anuluj");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
