package Interface;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JCalendar;
import com.toedter.components.JSpinField;

import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Date;

public class Cleaner extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JCalendar calendar;
	
	private Kalendarz kalendarzRef;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			Cleaner dialog = new Cleaner();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public Cleaner(JFrame owner, Kalendarz kalendarz) {
		super(owner, "Filtruj");
		this.kalendarzRef = kalendarz;
		setBounds(100, 100, 450, 300);
		setVisible(true);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		calendar = new JCalendar();
		contentPanel.add(calendar);

		JLabel lblGodzina = new JLabel("Godzina:");
		contentPanel.add(lblGodzina);

		final JSpinner spinnerHours = new JSpinner();
		spinnerHours.setModel(new SpinnerNumberModel(0, 0, 24, 1));
		contentPanel.add(spinnerHours);

		JLabel lblMinuta = new JLabel("Minuta:");
		contentPanel.add(lblMinuta);

		final JSpinner spinnerMinutes = new JSpinner();
		spinnerMinutes.setModel(new SpinnerNumberModel(0, 0, 59, 1));
		contentPanel.add(spinnerMinutes);

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		{
			JButton okButton = new JButton("Usu\u0144");
			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Date date = calendar.getDate();
					date.setHours((Integer) spinnerHours.getValue());
					date.setMinutes((Integer) spinnerMinutes.getValue());
					kalendarzRef.cleanByDate(date);
					dispose();
				}
			});
			okButton.setActionCommand("OK");
			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
		}
		{
			JButton cancelButton = new JButton("Anuluj");
			cancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			cancelButton.setActionCommand("Cancel");
			buttonPane.add(cancelButton);
		}
	}
}


