package Logic;

import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import Data.EventContainer;

public class ConverterXML implements DAOInterface {

	public void save(EventContainer events) {
		XStream xstream = new XStream();
		xstream.alias("events", EventContainer.class);
		String eventsXML = xstream.toXML(events);
		try
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter("OrganizerData.xml"));
			writer.write(eventsXML);
			writer.flush();
			writer.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public EventContainer load() {
		EventContainer events = null;
		XStream xstream = new XStream(new DomDriver());
		xstream.alias("events", EventContainer.class);
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader("OrganizerData.xml"));
			events = (EventContainer)xstream.fromXML(reader);
			reader.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

		return events;
	}

}
