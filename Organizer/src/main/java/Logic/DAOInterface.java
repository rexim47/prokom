package Logic;

import java.io.File;

import Data.EventContainer;

public interface DAOInterface {
	public void save(EventContainer events);
	public EventContainer load();
}
