package Logic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import Data.Event;
import Data.EventContainer;

public class ConverterDB implements DAOInterface
{

	///////////////////
	//Pola
	///////////////////
	private String user;
	private String password;
	private String url;
	//Format daty
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public ConverterDB()
	{
		this.user = new String("kompot");
		this.password = new String ("topmok");
		this.url = new String("jdbc:mysql://localhost/kompot");
	}
	
	public ConverterDB(String user, String password, String url)
	{
		this.user = user;
		this.password = password;
		this.url = url;
	}
	
	public Connection getConnection()
	{
	    Connection connection = null;
		try 
		{
		    connection = DriverManager.getConnection(this.url,this.user,this.password);
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		
	    return connection;
	}

	public void save(EventContainer events) {
		clear(); //MEGA NIEOPTYMALNE PODEJSCIE Z CZYSZCZENIEM BAZY PRZY KAZDYM ZAPISIE
		try
		{
			Statement statement = this.getConnection().createStatement();
			//for(int i=(selectNextId());i<events.size();i++)
			for(int i=0;i<events.size();i++)
			{
				//Trzeba zformatowac date do stringa do MySQL DATETIME
				String eventDate = sdf.format(events.getEvent(i).getDate());
				String eventReminderDate = sdf.format(events.getEvent(i).getReminderDate());
				statement.executeUpdate("INSERT INTO events VALUES (" + events.getEvent(i).getId() 
						+ ",'" + events.getEvent(i).getTitle() 
						+ "','" + events.getEvent(i).getDescription() 
						+ "','" + events.getEvent(i).getLocalization() 
						+ "','" + eventDate 
						+ "','" + eventReminderDate
						+ "'," + events.getEvent(i).getReminderStatus() + ")");
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		
	}

	public EventContainer load() {
		EventContainer events = new EventContainer();
		try
		{
			Statement statement = this.getConnection().createStatement();
			ResultSet results = statement.executeQuery("SELECT * FROM events");
			while(results.next())
			{
				Event event = new Event();
				Date eventDate = new Date();
				Date eventReminderDate = new Date();
				try {
					eventDate = sdf.parse(results.getString("date"));
					eventReminderDate = sdf.parse(results.getString("reminderDate"));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				event.setId(results.getInt("id"));
				event.setTitle(results.getString("title"));
				event.setDescription(results.getString("description"));
				event.setLocalization(results.getString("localization"));
				event.setDate(eventDate);
				event.setReminderDate(eventReminderDate);
				event.setReminderStatus(results.getBoolean("reminderStatus"));
				
				events.addEvent(event);
			}
			events.setNextId();
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		return events;
	}
	
	private int selectNextId()
	{
		int id = 0;
		try
		{
			Statement statement = this.getConnection().createStatement();
			ResultSet results = statement.executeQuery("SELECT COUNT(*) FROM events");
			results.next();
			if(results.getInt("COUNT(*)") != 0)
			{
				results = statement.executeQuery("SELECT id FROM events");
				results.last();
				id = results.getInt("id")+1;
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		return id;
	}
	
	private void clear()
	{
		try
		{
			Statement statement = this.getConnection().createStatement();
			statement.executeUpdate("DELETE FROM events WHERE 1");
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
}
