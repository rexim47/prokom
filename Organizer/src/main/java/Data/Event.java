package Data;
import java.util.Date;


public class Event 
{
	///////////////////
	//Pola
	///////////////////
	private int id;
	private String title;
	private String description;
	private String localization;
	private Date date;
	private Date reminderDate;
	private boolean reminderStatus = false;
	
	///////////////////
	//Konstruktory
	///////////////////
	public Event(int id, String title, String description, String localization, Date date, Date reminderDate, boolean reminderStatus)
	{
		this.id = id;
		this.title = title;
		this.description = description;
		this.localization = localization;
		this.date = date;
		this.reminderDate = reminderDate;
		this.reminderStatus = reminderStatus;
	}
	
	public Event() {
		// TODO Auto-generated constructor stub
	}

	///////////////////
	//Akcesory
	///////////////////
	///////////////////
	//Set
	///////////////////
	public void setId(int id)
	{
		this.id = id;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public void setLocalization(String localization)
	{
		this.localization = localization;
	}
	
	public void setDate(Date date)
	{
		this.date = date;
	}
	
	public void setReminderDate(Date reminderDate)
	{
		this.reminderDate = reminderDate;
	}
	
	public void setReminderStatus(boolean value)
	{
		this.reminderStatus = value;
	}
	
	public void setAll(int id, String title, String description, String localization, Date date, Date reminderDate)
	{
		this.id = id;
		this.title = title;
		this.description = description;
		this.localization = localization;
		this.date = date;
		this.reminderDate = reminderDate;
	}
	
	///////////////////
	//Get
	///////////////////
	public int getId()
	{
		return this.id;
	}
	
	public String getTitle()
	{
		return this.title;
	}
	
	public String getDescription()
	{
		return this.description;
	}
	
	public String getLocalization()
	{
		return this.localization;
	}
	
	public Date getDate()
	{
		return this.date;
	}
	
	public Date getReminderDate()
	{
		return this.reminderDate;
	}
	
	public boolean getReminderStatus()
	{
		return this.reminderStatus;
	}
	
	public String toString()
	{
		return this.getTitle() + " " + this.getDescription() + " " + this.getLocalization() + " " + this.getDate();
	}
}
