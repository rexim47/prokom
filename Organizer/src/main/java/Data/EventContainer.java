package Data;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class EventContainer 
{
	///////////////////
	//Pola
	///////////////////
	private int nextId;
	private ArrayList<Event> events;

	///////////////////
	//Konstruktory
	///////////////////
	public EventContainer()
	{
		this.nextId=0;
		this.events = new ArrayList<Event>();
	}
	
	///////////////////
	//Akcesory
	///////////////////
	public void setNextId(int NextId)
	{
		this.nextId = NextId;
	}
	
	public int getNextId()
	{
		return this.nextId;
	}
	
	public void setNextId()
	{
		this.nextId = size();
	}
	
	public void addEvent(Event event)
	{
		events.add(event);
		nextId++;
	}
	
	public void removeEvent(Event event)
	{
		events.remove(event);
	}
	
	public void removeEvent(int id)
	{
		events.remove(id);
	}
	
	public void replaceEvent(int id, Event event)
	{
		events.get(id).setAll(event.getId(), 
				event.getTitle(),
				event.getDescription(), 
				event.getLocalization(),
				event.getDate(), 
				event.getReminderDate());
	}
	
	public Event getEvent(int id)
	{
		return events.get(id);
	}
	
	public ArrayList<Event> getEvents()
	{
		return this.events;
	}
	
	public int size()
	{
		return events.size();
	}
	
	public String toString()
	{
		String result = "";
		for(int i=0;i<size();i++)
			result += getEvent(i).toString() + "\n";
		return result;
	}
	
	public EventContainer selectEventsByDate(Date date)
	{
		EventContainer temp = new EventContainer();
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		for(int i=0;i<size();i++)
			if(events.get(i).getDate().getYear() == date.getYear())
				if(events.get(i).getDate().getMonth() == date.getMonth())
				{
					cal1.setTime(events.get(i).getDate());
					cal2.setTime(date);
					if(cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH))
						temp.addEvent(events.get(i));
				}
		return temp;
	}
	
	public EventContainer selectEventsByTitle(String title)
	{
		EventContainer temp = new EventContainer();
		for(int i=0;i<size();i++)
			if(events.get(i).getTitle().compareTo(title) == 0)
				temp.addEvent(events.get(i));
		return temp;
	}
	
	public EventContainer selectEventsByLocalization(String localization)
	{
		EventContainer temp = new EventContainer();
		for(int i=0;i<size();i++)
			if(events.get(i).getLocalization().compareTo(localization) == 0)
				temp.addEvent(events.get(i));
		return temp;
	}
	
	public void cleanEventsByDate(Date date)
	{
		EventContainer eventsToRemove = new EventContainer();
		for(int i=0;i<events.size();i++)
			if(events.get(i).getDate().before(date))
			//if(events.get(i).getDate().getTime()-(date.getTime())<0)
				eventsToRemove.addEvent(events.get(i));

		for(int i=0;i<eventsToRemove.size();i++)
			removeEvent(eventsToRemove.getEvent(i));
	}
	
	public Event selectEventWithReminder(Date date)
	{
		Event temp = new Event();
		for(int i=0;i<size();i++)
		{
			temp = events.get(i);
//			if(events.get(i).getReminderDate().getYear() == date.getYear())
//				if(events.get(i).getReminderDate().getMonth() == date.getMonth())
//					if(events.get(i).getReminderDate().getDay() == date.getDay())
//						if(events.get(i).getReminderDate().getHours() == date.getHours())
//							if(events.get(i).getReminderDate().getMinutes() > date.getMinutes() && events.get(i).getDate().getMinutes() > date.getMinutes())
			//if(!temp.getDate().equals(temp.getReminderDate()))
			if(temp.getReminderStatus())
			{
				int diff1 = (int) (date.getTime() - events.get(i).getReminderDate().getTime());
				int diff2 = (int) (events.get(i).getDate().getTime() - events.get(i).getReminderDate().getTime());
				//System.out.println("diff1 = " + diff1 + " diff2 = " + diff2);
				if(diff1>=0 && diff1 <= diff2)
				{
					temp.setReminderStatus(false);
					return temp;
				}
			}
		}
		return null;
	}
	
	public void setAll(EventContainer newEvents)
	{
		this.nextId = newEvents.getNextId();
		this.events = newEvents.getEvents();
	}
}