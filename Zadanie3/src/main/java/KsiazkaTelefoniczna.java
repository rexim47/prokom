import java.util.Arrays;


public class KsiazkaTelefoniczna 
{
	private int rozmiar;
	private Wpis [] wpisy = new Wpis [1];
	
	public KsiazkaTelefoniczna()
	{
		wpisy[0] = null;
		rozmiar = 0;
	}
	
	public void appendElement(Wpis w)
	{
		rozmiar++;
		if(rozmiar == 1)
			wpisy[0] = w;
		else
		{
			wpisy = Arrays.copyOf(wpisy, rozmiar);
			wpisy[rozmiar-1] = w;
		}
	}
	
	public void removeElement(int remove)
	{
		if(rozmiar > 0)
		{
			for(int i=remove;i<rozmiar-1;i++)
				wpisy[i] = wpisy[i+1];
			rozmiar--;
			wpisy = Arrays.copyOf(wpisy, rozmiar);
		}
	}
	
	public void sort()
	{
		Arrays.sort(wpisy);
	}
	
	public void sort(Komparator c)
	{
		Arrays.sort(wpisy,c);
	}
	public Wpis getElement(int pos)
	{
		return wpisy[pos];
	}
	
	public int size()
	{
		return wpisy.length;
	}
	
	public String toString()
	 {
	  String tekst="";
	  for(int i=0; i<rozmiar; i++)
	   tekst+=wpisy[i].toString()+"\n";
	  return tekst;
	 }
}
